!!!<Number_of_UEs:	100>!!!

eval.showNumMs = true;
eval.showQos = true;
eval.showQosSplit = true;

Number_of_micro-services:	3
QoS_split_-_data_rate:	2
QoS_split_-_latency:	0.85
QoS_split_-_jitter:	0.85
Number_of_RRHs:	3
Cell_size_[meter]:	20
Number_of_cloud_nodes:	60
Number_of_connection_per_cloud_node:	10
Number_of_UEs:	100
Node_capacity_-_CPU_[MIPS]:	2400
Node_capacity_-_RAM_[MByte]:	6000
Node_capacity_-_Flash_[GByte]:	1024
Edge_capacity_-_bandwidth_[Mbps]:	1000
Node_requirement_-_CPU_[MIPS]:	2
Node_requirement_-_RAM_[MByte]:	60
Node_requirement_-_Flash_[GByte]:	1
Edge_requirement_-_bandwidth_[Mbps]:	0.1
QoS_-_data_rate_[Mbps]:	0.0664
QoS_-_latency_[milliseconds]:	1.35
QoS_-_jitter_[millisedonds]:	1.35
QoS_-_message_size_[Bytes]:	83
Node_performance_-_data_rate_[Mbps]:	1.79769e+308
Node_performance_-_latency_[milliseconds]:	0.1
Node_performance_-_jitter_[millisedonds]:	0.1
Edge_performance_-_data_rate_[Mbps]:	1000
Edge_performance_-_latency_[milliseconds]:	0.05
Edge_performance_-_jitter_[milliseconds]:	0.05
