#include <iostream>
#include <stdint.h>
#include <array>
#include <vector>
#include <map>
#include <random>
#include <iostream>

#include "simulation.h"

int main(int argc, char **argv) {

	EvalProfile eval;
	eval.ReadFromFile("conf.txt");
	eval.numRrhs = 3;
	eval.cellSize = 30;
	eval.numAgvs = 2000;
	eval.appQos.jitter = 100000;
	eval.appQos.latency = 10.0;
	eval.appQos.datarate = 0.04;
	eval.appQos.messageSize = 83;
	eval.qosSplit = QosSplit(2.0, 0.5, 0.5);
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.numCloudNodes = 70;
	eval.numConnectionPerCloudNode = 5;
	eval.numMicroServices = 5; // not counting the fake micro-services
	eval.nodeRequirement = NodeRequirement(1, 1, 1);
	eval.edgeRequirement = EdgeRequirement(1);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);
	eval.admissionP = 0.999;
	eval.dynamicBalancing = true;

	eval.showNumRrhs = true;
	eval.showNumAgvs = true;
	eval.showCellSize = true;
	eval.showNumCloudNodes = true;
	eval.showNumConnPerCloudNode = true;
	eval.showNumMs = true;
	eval.showQos = true;
	eval.showAdmissionP = true;
	eval.showQosSplit = true;

	eval.admissionP = 0.999;
	eval.qosSplit.sLatency = 0.5;
	eval.qosSplit.sDatarate = 2.0;

	Simulation sim(eval);
	sim.Run();
	sim.PrintSummaryShort();

	std::cout << "Finished" << std::endl;

	return 0;
}
