#include <iostream>
#include <stdint.h>
#include <array>
#include <vector>
#include <map>
#include <random>
#include <iostream>

#include "simulation.h"

int main(int argc, char **argv) {

	EvalProfile eval;
	eval.ReadFromFile("conf.txt");
	eval.numRrhs = 3;
	eval.cellSize = 30;
	eval.numAgvs = 2000;
	eval.appQos.jitter = 100000;
	eval.appQos.latency = 2;
	eval.appQos.datarate = 0.064;
	eval.appQos.messageSize = 83;
	eval.showNumRrhs = true;
	eval.showNumAgvs = true;
	eval.showCellSize = true;
	eval.showQos = true;

	std::vector<double> datarates = { 0.04, 0.08, 0.16, 0.32, 0.64, 1.28};
	std::vector<double> pamins = { 0.999};

	for (auto datarate : datarates) {

		Statistics st;
		uint32_t numIter = NUM_CONF_INTERV_SAMPLES;
		struct StatsPa {
			std::vector<uint32_t> numJoined, numUntilLastJoined, numJoinedUntilPa;
			std::vector<double> ranUsage;
		};
		std::vector<StatsPa> statsPa(pamins.size(), StatsPa());

		for (uint32_t i = 0; i < numIter; i++) {

			eval.appQos.datarate = datarate;

			RanManager ran(eval);
			AgvProfile agvProfile(eval);
			ran.InstallApp(agvProfile);
			UeManager uem(eval);

			uint32_t numJoined = 0;
			uint32_t numJoinedUntilPa = 0;
			uint32_t numUntilLastJoined = 0;
			double pa = 0;

			auto pamin = pamins.begin();
			uint32_t c = 0;

			while (!uem.HaveAllTriedToJoin()) {

				c++;

				auto reqMdmo = uem.CreateRequest();
				InquiryRan inqRan(reqMdmo.agv);
				inqRan.t = reqMdmo.t;

				auto respRan = ran.HandleInquiry(inqRan);
				if (respRan.success) {
					assert(!respRan.options.empty());
					RequestRan req(reqMdmo.agv);
					ResoruceReservationPolicy policy;
					req.option =  policy.SelectOption(respRan.options);
					req.t = reqMdmo.t;
					ran.HandleRequest(req);
					numJoined++;
					numUntilLastJoined = c;
				} else {
//				std::cout << *pamin << "\t" << numJoined << "\t" << numUntilLastJoined << "\t" << numJoinedUntilPa << "\t" << eval << "\t"
//						<< ran.GetResourceUsage() << std::endl;
//				ran.PrintResources();
//				exit(0);
				}

				pa = (double) numJoined / (double) c;

				if (respRan.success && pa > *pamin) {
					numJoinedUntilPa = numJoined;
				}
				if (pa < *pamin) {

					statsPa.at(std::distance(pamins.begin(), pamin)).numJoined.push_back(numJoined);
					statsPa.at(std::distance(pamins.begin(), pamin)).numUntilLastJoined.push_back(numUntilLastJoined);
					statsPa.at(std::distance(pamins.begin(), pamin)).numJoinedUntilPa.push_back(numJoinedUntilPa);
					statsPa.at(std::distance(pamins.begin(), pamin)).ranUsage.push_back(ran.GetResourceUsage());
//					std::cout << *pamin << "\t" << numJoined << "\t" << numUntilLastJoined << "\t" << numJoinedUntilPa << "\t" << eval << "\t"
//							<< ran.GetResourceUsage() << std::endl;
					pamin++;
					if (pamin == pamins.end()) {
						break;
					}
				}
			}

		}

		for (uint16_t i = 0; i < pamins.size(); i++) {
			auto numJoined = st.CalcCI(statsPa.at(i).numJoined);
			auto numUntilLastJoined = st.CalcCI(statsPa.at(i).numUntilLastJoined);
			auto numJoinedUntilPa = st.CalcCI(statsPa.at(i).numJoinedUntilPa);
			auto ranUsage = st.CalcCI(statsPa.at(i).ranUsage);
			std::cout << pamins.at(i) << "\t" << numJoined << "\t" << numUntilLastJoined << "\t" << numJoinedUntilPa << "\t" << eval << "\t" << ranUsage
					<< std::endl;
		}
	}
	std::cout << "Finished" << std::endl;

	return 0;
}
