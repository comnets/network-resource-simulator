################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../test/access-plan-test.cpp \
../test/can-accomodate.cpp \
../test/channel.cpp \
../test/cloud-app-deploy.cpp \
../test/create-cloud.cpp \
../test/dll-test.cpp \
../test/full-sim.cpp \
../test/main.cpp \
../test/resources-fill.cpp \
../test/resources-match-rb.cpp 

OBJS += \
./test/access-plan-test.o \
./test/can-accomodate.o \
./test/channel.o \
./test/cloud-app-deploy.o \
./test/create-cloud.o \
./test/dll-test.o \
./test/full-sim.o \
./test/main.o \
./test/resources-fill.o \
./test/resources-match-rb.o 

CPP_DEPS += \
./test/access-plan-test.d \
./test/can-accomodate.d \
./test/channel.d \
./test/cloud-app-deploy.d \
./test/create-cloud.d \
./test/dll-test.d \
./test/full-sim.d \
./test/main.d \
./test/resources-fill.d \
./test/resources-match-rb.d 


# Each subdirectory must supply rules for building sources it contributes
test/%.o: ../test/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


