################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/cloud-performance.cpp \
../src/e2e-performance.cpp \
../src/example.cpp \
../src/main.cpp \
../src/mylib.cpp \
../src/ran-performance.cpp 

OBJS += \
./src/cloud-performance.o \
./src/e2e-performance.o \
./src/example.o \
./src/main.o \
./src/mylib.o \
./src/ran-performance.o 

CPP_DEPS += \
./src/cloud-performance.d \
./src/e2e-performance.d \
./src/example.d \
./src/main.d \
./src/mylib.d \
./src/ran-performance.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


