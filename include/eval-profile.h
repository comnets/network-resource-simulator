/*
 * eval-profile.h
 *
 *  Created on: Mar 10, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_EVAL_PROFILE_H_
#define INCLUDE_EVAL_PROFILE_H_

#include <stdint.h>
#include <fstream>
#include <string.h>
#include <sstream>

struct EvalProfile {

	EvalProfile() {
		qosSplit = QosSplit(2, 0.5, 0.5);
		numMicroServices = 1;
		numRrhs = 3;
		cellSize = 20;
		numCloudNodes = 100;
		numConnectionPerCloudNode = 5;
		numAgvs = 100;
		nodeCapacity = NodeResource(100, 100, 100);
		edgeCapacity = EdgeResource(100);
		nodeRequirement = NodeRequirement(30, 30, 30);
		edgeRequirement = EdgeRequirement(30);
		appQos = QoS(83, 0.0664, 2, 2);
		nodePerformance = Performance(0, 3000, 0.1, 0.1);
		edgePerformance = Performance(0, 1000, 0.05, 0.05);
		admissionP = 0.999;
		dynamicBalancing = false;

		showQosSplit = false;
		showNumMs = false;
		showNodeCap = false;
		showEdgeCap = false;
		showQos = false;
		showNodeReq = false;
		showEdgeReq = false;
		showNodePerf = false;
		showEdgePerf = false;
		showNumRrhs = false;
		showCellSize = false;
		showNumCloudNodes = false;
		showNumConnPerCloudNode = false;
		showNumAgvs = false;
		showAdmissionP = false;
		showDynamicBalancing = false;
	}

	void ReadFromFile(std::string filename) {
		std::ifstream ifs;
		ifs.open("../" + filename, std::ifstream::in);
		// "[^"]+"
		std::string caption;
		ifs >> caption >> numMicroServices;
		ifs >> caption >> qosSplit.sDatarate;
		ifs >> caption >> qosSplit.sLatency;
		ifs >> caption >> qosSplit.sJitter;
		ifs >> caption >> numRrhs;
		ifs >> caption >> cellSize;
		ifs >> caption >> numCloudNodes;
		ifs >> caption >> numConnectionPerCloudNode;
		ifs >> caption >> numAgvs;
		ifs >> caption >> nodeCapacity.cpu;
		ifs >> caption >> nodeCapacity.ram;
		ifs >> caption >> nodeCapacity.flash;
		ifs >> caption >> edgeCapacity.bandwidth;
		ifs >> caption >> nodeRequirement.cpu;
		ifs >> caption >> nodeRequirement.ram;
		ifs >> caption >> nodeRequirement.flash;
		ifs >> caption >> edgeRequirement.bandwidth;
		ifs >> caption >> appQos.datarate;
		ifs >> caption >> appQos.latency;
		ifs >> caption >> appQos.jitter;
		ifs >> caption >> appQos.messageSize;
		ifs >> caption >> nodePerformance.datarate;
		ifs >> caption >> nodePerformance.latency;
		ifs >> caption >> nodePerformance.jitter;
		ifs >> caption >> edgePerformance.datarate;
		ifs >> caption >> edgePerformance.latency;
		ifs >> caption >> edgePerformance.jitter;
		ifs >> caption >> admissionP;
		ifs >> caption >> dynamicBalancing;

		ifs.close();
	}

	void WriteToFile(std::string filename) {

		std::ofstream ofs;
		ofs.open("../" + filename, std::ofstream::out);

		ofs << "Number_of_micro-services:\t" << numMicroServices << "\n";
		ofs << "QoS_split_-_data_rate:\t" << qosSplit.sDatarate << "\n";
		ofs << "QoS_split_-_latency:\t" << qosSplit.sLatency << "\n";
		ofs << "QoS_split_-_jitter:\t" << qosSplit.sJitter << "\n";
		ofs << "Number_of_RRHs:\t" << numRrhs << "\n";
		ofs << "Cell_size_[meter]:\t" << cellSize << "\n";
		ofs << "Number_of_cloud_nodes:\t" << numCloudNodes << "\n";
		ofs << "Number_of_connection_per_cloud_node:\t" << numConnectionPerCloudNode << "\n";
		ofs << "Number_of_UEs:\t" << numAgvs << "\n";
		ofs << "Node_capacity_-_CPU_[MIPS]:\t" << nodeCapacity.cpu << "\n";
		ofs << "Node_capacity_-_RAM_[MByte]:\t" << nodeCapacity.ram << "\n";
		ofs << "Node_capacity_-_Flash_[GByte]:\t" << nodeCapacity.flash << "\n";
		ofs << "Edge_capacity_-_bandwidth_[Mbps]:\t" << edgeCapacity.bandwidth << "\n";
		ofs << "Node_requirement_-_CPU_[MIPS]:\t" << nodeRequirement.cpu << "\n";
		ofs << "Node_requirement_-_RAM_[MByte]:\t" << nodeRequirement.ram << "\n";
		ofs << "Node_requirement_-_Flash_[GByte]:\t" << nodeRequirement.flash << "\n";
		ofs << "Edge_requirement_-_bandwidth_[Mbps]:\t" << edgeRequirement.bandwidth << "\n";
		ofs << "QoS_-_data_rate_[Mbps]:\t" << appQos.datarate << "\n";
		ofs << "QoS_-_latency_[milliseconds]:\t" << appQos.latency << "\n";
		ofs << "QoS_-_jitter_[millisedonds]:\t" << appQos.jitter << "\n";
		ofs << "QoS_-_message_size_[Bytes]:\t" << appQos.messageSize << "\n";
		ofs << "Node_performance_-_data_rate_[Mbps]:\t" << nodePerformance.datarate << "\n";
		ofs << "Node_performance_-_latency_[milliseconds]:\t" << nodePerformance.latency << "\n";
		ofs << "Node_performance_-_jitter_[millisedonds]:\t" << nodePerformance.jitter << "\n";
		ofs << "Edge_performance_-_data_rate_[Mbps]:\t" << edgePerformance.datarate << "\n";
		ofs << "Edge_performance_-_latency_[milliseconds]:\t" << edgePerformance.latency << "\n";
		ofs << "Edge_performance_-_jitter_[milliseconds]:\t" << edgePerformance.jitter << "\n";
		ofs << "Minimal admission probability:\t" << admissionP << "\n";
		ofs << "Dynamic balancing of QoS requirements:\t" << dynamicBalancing << "\n";

		ofs.close();
	}

	friend std::ostream& operator<<(std::ostream &os, const EvalProfile &obj) {
		if (obj.showNumMs) os << obj.numMicroServices << "\t";
		if (obj.showQosSplit) os << obj.qosSplit.sDatarate << "\t";
		if (obj.showQosSplit) os << obj.qosSplit.sLatency << "\t";
		if (obj.showQosSplit) os << obj.qosSplit.sJitter << "\t";
		if (obj.showNumRrhs) os << obj.numRrhs << "\t";
		if (obj.showCellSize) os << obj.cellSize << "\t";
		if (obj.showNumCloudNodes) os << obj.numCloudNodes << "\t";
		if (obj.showNumConnPerCloudNode) os << obj.numConnectionPerCloudNode << "\t";
		if (obj.showNumAgvs) os << obj.numAgvs << "\t";
		if (obj.showNodeCap) os << obj.nodeCapacity.cpu << "\t";
		if (obj.showNodeCap) os << obj.nodeCapacity.ram << "\t";
		if (obj.showNodeCap) os << obj.nodeCapacity.flash << "\t";
		if (obj.showEdgeCap) os << obj.edgeCapacity.bandwidth << "\t";
		if (obj.showNodeReq) os << obj.nodeRequirement.cpu << "\t";
		if (obj.showNodeReq) os << obj.nodeRequirement.ram << "\t";
		if (obj.showNodeReq) os << obj.nodeRequirement.flash << "\t";
		if (obj.showEdgeReq) os << obj.edgeRequirement.bandwidth << "\t";
		if (obj.showQos) os << obj.appQos.datarate << "\t";
		if (obj.showQos) os << obj.appQos.latency << "\t";
		if (obj.showQos) os << obj.appQos.jitter << "\t";
		if (obj.showQos) os << obj.appQos.messageSize << "\t";
		if (obj.showNodePerf) os << obj.nodePerformance.datarate << "\t";
		if (obj.showNodePerf) os << obj.nodePerformance.latency << "\t";
		if (obj.showNodePerf) os << obj.nodePerformance.jitter << "\t";
		if (obj.showEdgePerf) os << obj.edgePerformance.datarate << "\t";
		if (obj.showEdgePerf) os << obj.edgePerformance.latency << "\t";
		if (obj.showEdgePerf) os << obj.edgePerformance.jitter << "\t";
		if (obj.showAdmissionP) os << obj.admissionP << "\t";
		if (obj.showDynamicBalancing) os << obj.dynamicBalancing << "\t";
		return os;
	}

	EvalProfile& operator=(const EvalProfile &obj) {
		if (&obj == this) return *this;
		numMicroServices = obj.numMicroServices;
		qosSplit.sDatarate = obj.qosSplit.sDatarate;
		qosSplit.sLatency = obj.qosSplit.sLatency;
		qosSplit.sJitter = obj.qosSplit.sJitter;
		numRrhs = obj.numRrhs;
		cellSize = obj.cellSize;
		numCloudNodes = obj.numCloudNodes;
		numConnectionPerCloudNode = obj.numConnectionPerCloudNode;
		numAgvs = obj.numAgvs;
		nodeCapacity = obj.nodeCapacity;
		edgeCapacity = obj.edgeCapacity;
		nodeRequirement = obj.nodeRequirement;
		edgeRequirement = obj.edgeRequirement;
		appQos = obj.appQos;
		nodePerformance = obj.nodePerformance;
		edgePerformance = obj.edgePerformance;
		admissionP = obj.admissionP;
		dynamicBalancing = obj.dynamicBalancing;
		showQosSplit = obj.showQosSplit;
		showNumMs = obj.showNumMs;
		showNodeCap = obj.showNodeCap;
		showEdgeCap = obj.showEdgeCap;
		showQos = obj.showQos;
		showNodeReq = obj.showNodeReq;
		showEdgeReq = obj.showEdgeReq;
		showNodePerf = obj.showNodePerf;
		showEdgePerf = obj.showEdgePerf;
		showNumRrhs = obj.showNumRrhs;
		showCellSize = obj.showCellSize;
		showNumCloudNodes = obj.showNumCloudNodes;
		showNumConnPerCloudNode = obj.showNumConnPerCloudNode;
		showNumAgvs = obj.showNumAgvs;
		showAdmissionP = obj.showAdmissionP;
		showDynamicBalancing = obj.showDynamicBalancing;
		return *this;
	}

	QosSplit qosSplit;
	uint32_t numMicroServices; // not counting the fake micro-services
	NodeResource nodeCapacity;
	EdgeResource edgeCapacity;
	QoS appQos;
	NodeRequirement nodeRequirement;
	EdgeRequirement edgeRequirement;
	Performance nodePerformance;
	Performance edgePerformance;
	uint32_t numRrhs;
	double cellSize;
	uint32_t numCloudNodes;
	uint32_t numConnectionPerCloudNode;
	uint32_t numAgvs;
	double admissionP;
	bool dynamicBalancing;

	bool showQosSplit;
	bool showNumMs;
	bool showNodeCap;
	bool showEdgeCap;
	bool showQos;
	bool showNodeReq;
	bool showEdgeReq;
	bool showNodePerf;
	bool showEdgePerf;
	bool showNumRrhs;
	bool showCellSize;
	bool showNumCloudNodes;
	bool showNumConnPerCloudNode;
	bool showNumAgvs;
	bool showAdmissionP;
	bool showDynamicBalancing;
};

#endif /* INCLUDE_EVAL_PROFILE_H_ */
