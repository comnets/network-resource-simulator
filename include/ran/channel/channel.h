/*
 * channel.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CHANNEL_H_
#define INCLUDE_CHANNEL_H_

#include <cmath>
#include <math.h>

//#include "effective-snr.h"
#include "resources.h"
#include "log.h"

/*
 * https://www.nsnam.org/doxygen/classns3_1_1_log_distance_propagation_loss_model.html#details
 */

struct Position {
	Position() {
		x = 0;
		y = 0;
	}
	Position(double x, double y) {
		this->x = x;
		this->y = y;
	}

	Position& operator=(const Position &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		x = obj.x;
		y = obj.y;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, const Position &obj) {
		os << "[" << obj.x << "," << obj.y << "]";
		return os;
	}

	double x;
	double y;
};
struct Power {
	Power() : power(Carriers::maxRe, 0) {
	}
	double Get(uint32_t i) {
		return power.at(i);
	}
	void Set(double v, uint32_t i) {
		assert(i < power.size());
		power.at(i) = v;
	}
	void Add(double v, uint32_t i) {
		assert(i < power.size());
		power.at(i) += v;
	}

	Power& operator+=(const Power &rhs) {
		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
			this->power.at(i) += rhs.power.at(i);
		}
		return *this;
	}
	Power& operator=(const Power &obj) {
		if (&obj == this) return *this;
		this->power = obj.power;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, const Power &obj) {
		os << "[";
		for (auto v : obj.power)
			os << v << ",";
		os << "]";
		return os;
	}
private:
	std::vector<double> power;
};
struct Snr {
	Snr() : snr(Carriers::maxRe, 0), usage(Carriers::maxRe, false) {
	}

	double Get(uint32_t i) {
		return snr.at(i);
	}
	void Set(double v, uint32_t i) {
		assert(i < snr.size());
		snr.at(i) = v;
		usage.at(i) = true;
	}
	uint32_t Size() {
		return Carriers::maxRe;
	}

	Power GetPower() {
		Power p;
		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
			p.Set(pow(10, snr.at(i) / 10.0), i);
		}
		return p;
	}

	void AddNoise(Power noise) {
		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
			snr.at(i) -= 10 * log10(1.0 + noise.Get(i));
		}
	}

	bool IsInUse(uint32_t i) {
		assert(i < usage.size());
		return usage.at(i);
	}
	Snr& operator=(const Snr &obj) {
		if (&obj == this) return *this;
		this->snr = obj.snr;
		this->usage = obj.usage;
		return *this;
	}
	friend std::ostream& operator<<(std::ostream &os, const Snr &obj) {
		os << "[";
		for (uint32_t i = 0; i < obj.snr.size(); i++) {
			os << obj.snr.at(i) << "|" << obj.usage.at(i) << ",";
		}
		os << "]";
		return os;
	}

private:

	std::vector<double> snr;
	std::vector<bool> usage;
};

class RanPtpChannel {
public:
	RanPtpChannel() {
		m_refDistance = 1;
		m_refSnr = 46.6777;
		m_lossExp = 3;
		m_txP = -47.5;
		m_awgnPD = -174;
		m_bandwidth = 20;
		m_awgnP = m_awgnPD + 10 * log10(m_bandwidth);
	}
	~RanPtpChannel() {
	}

	Snr GetSnr(Position src, Position dst) {
		Snr snr;
		auto distance = std::sqrt(std::pow(src.x - dst.x, 2) + std::pow(src.y - dst.y, 2));
		auto logNormAtt = GetLogNormSnr(distance);
		auto multipathAtt = GetMultipathAttenuation();
		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
			snr.Set(logNormAtt + multipathAtt.at(i), i);
		}
		return snr;
	}

private:

	double GetLogNormAttenuation(double distance) {
		if (distance < m_refDistance) return m_refSnr;
		return m_refSnr + 10 * m_lossExp * std::log10(distance / m_refDistance);
	}

	double GetLogNormSnr(double distance) {
		auto att = GetLogNormAttenuation(distance);
		RM_LOG(CHANNEL_LOG,
				"TX " << m_txP << " dBm / Attentuation " << att << " dB / White noise " << m_awgnP << " dBm / SNR " << m_txP - att - m_awgnP << " dB");
		return m_txP - att - m_awgnP;
	}

	std::vector<double> GetMultipathAttenuation() {
		return std::vector<double>(Carriers::maxRe, 0);
	}

	double m_refDistance; // unit [meter]
	double m_refSnr; // unit [dB]
	double m_lossExp; // no unit
	double m_txP; // unit [dBm]
	double m_awgnPD; // unit [dBm / Hz]
	double m_bandwidth; // [MHz]
	double m_awgnP; // unit [dBm]

};
//
//struct CommunicationPair {
//
//	CommunicationPair() {
//	}
//	CommunicationPair(Position src, Position dst, CarriersUsage cu) {
//		this->src = src;
//		this->dst = dst;
//		this->cu = cu;
//	}
//	CommunicationPair(double srcX, double srcY, double dstX, double dstY) : src(srcX, srcY), dst(dstX, dstY) {
//	}
//	Position src;
//	Position dst;
//	CarriersUsage cu;
//};
//
//struct Interferent {
//	Interferent(Position pos, CarriersUsage cu) {
//		this->pos = pos;
//		this->cu = cu;
//	}
//	Interferent() {
//	}
//	Position pos;
//	CarriersUsage cu;
//};
//
//class RanChannel {
//
//public:
//	RanChannel() {
//	}
//
//	~RanChannel() {
//	}
//
//	//
//	// consider a symmetric channel
//	//
//	double GetSnr(CommunicationPair commPair, std::vector<Interferent> interferents) {
//
//		if (CHANNEL_LOG) {
//			std::cout << "TxRx " << commPair.src << " <-> " << commPair.dst << " | ";
//			for (auto interferent : interferents) {
//				std::cout << "Interferent at " << interferent.pos << ",";
//			}
//			std::cout << std::endl;
//			for (uint32_t i = 0; i < Carriers::maxRe; i++) {
//				std::cout << commPair.cu.at(i) << " -> ";
//				for (auto interferent : interferents) {
//					std::cout << interferent.cu.at(i) << ",";
//				}
//			}
//			std::cout << std::endl;
//		}
//
////		std::vector<bool> carrierUsage;
////		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
////			carrierUsage.push_back(commPair.cu.at(i));
////		}
//
////		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
////			if (!commPair.cu.at(i)) {
////				for (auto &interferent : interferents) {
////					interferent.carriers.Release(i);
////				}
////			}
////		}
//
//		std::vector<double> snrInterferentsAtSrc(Carriers::maxRe, 0), snrInterferentsAtDst(Carriers::maxRe, 0);
//
//		for (auto interferent : interferents) {
//			auto snr1 = m_ptpChannel.GetSnr(interferent.pos.x, interferent.pos.y, commPair.src.x, commPair.src.y);
//			auto snr2 = m_ptpChannel.GetSnr(interferent.pos.x, interferent.pos.y, commPair.dst.x, commPair.dst.y);
//
//			for (uint32_t i = 0; i < Carriers::maxRe; i++) {
//				if (interferent.carriers.IsReservedAt(i)) {
//					snrInterferentsAtSrc.at(i) += pow(10, snr1.at(i) / 10.0);
//					snrInterferentsAtDst.at(i) += pow(10, snr2.at(i) / 10.0);
//					RM_LOG(CHANNEL_LOG, "Inter. SNR at src for carrier group " << i << " val " << snr1.at(i));
//					RM_LOG(CHANNEL_LOG, "Inter. SNR at dst for carrier group " << i << " val " << snr2.at(i));
//				}
//			}
//		}
//
//		std::vector<double> snrCommPair(Carriers::maxRe, 0), sinrAtSrc(Carriers::maxRe, 0), sinrAtDst(Carriers::maxRe, 0);
//		auto snr = m_ptpChannel.GetSnr(commPair.src.x, commPair.src.y, commPair.dst.x, commPair.dst.y);
//
//		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
//			if (commPair.carriers.IsReservedAt(i)) {
//				snrCommPair.at(i) += snr.at(i);
//			}
//		}
//
//		for (uint32_t i = 0; i < Carriers::maxRe; i++) {
//			if (commPair.carriers.IsReservedAt(i)) {
//				sinrAtSrc.at(i) = snrCommPair.at(i) - 10 * log10(1.0 + snrInterferentsAtSrc.at(i));
//				RM_LOG(CHANNEL_LOG, "SINR at src for carrier group " << i << " val " << sinrAtSrc.at(i));
//				sinrAtDst.at(i) = snrCommPair.at(i) - 10 * log10(1.0 + snrInterferentsAtDst.at(i));
//				RM_LOG(CHANNEL_LOG, "SINR at dst for carrier group " << i << " val " << sinrAtDst.at(i));
//			}
//		}
//
//		auto effSinrAtSrc = EffectiveSnr::GetEffSnr(sinrAtSrc, carrierUsage);
//		auto effSinrAtDst = EffectiveSnr::GetEffSnr(sinrAtDst, carrierUsage);
//		auto effSnrCommPair = EffectiveSnr::GetEffSnr(snrCommPair, carrierUsage);
//
//		RM_LOG(CHANNEL_LOG, "SNR " << effSnrCommPair << " SINR at SRC " << effSinrAtSrc << " SINR at DST " << effSinrAtDst);
//
//		return effSinrAtSrc < effSinrAtDst ? effSinrAtSrc : effSinrAtDst;
//	}
//
//private:
//
//	RanPtpChannel m_ptpChannel;
//};

#endif /* INCLUDE_CHANNEL_H_ */
