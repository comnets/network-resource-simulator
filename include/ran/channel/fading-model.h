/*
 * fading-model.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_FADING_MODEL_H_
#define INCLUDE_FADING_MODEL_H_

#include "channel.h"
#include "mcs.h"

struct FadingModel {
	FadingModel(double radioCellSide) {
		this->radioCellSide = radioCellSide;
	}
	FadingModel& operator=(const FadingModel &fading) {
		// check for self-assignment
		if (&fading == this)
			return *this;
		radioCellSide = fading.radioCellSide;
		return *this;
	}
	double GetBitPerRE(double distance) {
		double txP = 90;
		auto snr = channel.GetSnr(90, distance);
		return mcs.GetEffBits(snr);
	}

	double GetSimpleBitPerRE(double distance) {
			return (distance > radioCellSide) ? 0 : (1 - distance / radioCellSide) * 10;
		}

	double radioCellSide;


	RanChannel channel;
	RanMcs mcs;
};



#endif /* INCLUDE_FADING_MODEL_H_ */
