/*
 * effective-snr.h
 *
 *  Created on: Mar 5, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_RAN_DLL_EFFECTIVE_SNR_H_
#define INCLUDE_RAN_DLL_EFFECTIVE_SNR_H_

#include <vector>
#include <cmath>
#include <iostream>

#include "channel.h"

struct EffectiveSnr {
	static const double GetEffSnr(Snr snr) {

		double sumsrn = 0;
		uint32_t numUsedCarriers = 0;

		for (uint32_t i = 0; i < snr.Size(); i++) {
			if (snr.IsInUse(i)) {
				sumsrn += snr.Get(i);
				numUsedCarriers++;
			}
		}
		return (numUsedCarriers == 0) ? 0 : sumsrn / (double) numUsedCarriers;
	}
};

#endif /* INCLUDE_RAN_DLL_EFFECTIVE_SNR_H_ */
