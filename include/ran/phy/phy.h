/*
 * phy.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_PHY_PHY_H_
#define INCLUDE_PHY_PHY_H_

#define OFDM_SYM_DURATION 0.00007143
#define FRONT_END_DELAY	0.0002

#include <assert.h>
#include <vector>
#include <map>
#include <array>
#include <cmath>
#include <math.h>

class RanPhy {
public:

	typedef std::pair<std::string, double> eff_pair;

	RanPhy() {

		//
		// "Downlink SNR to CQI Mapping for Different Multiple Antenna Techniques in LTE"
		// Mohammad Kawser, Nafiz Imtiaz Bin Hamid
		//

		efficiency = { //
				eff_pair("QPSK		78/1024", 0.1523), //
				eff_pair("QPSK 		120/1024", 0.2344), //
				eff_pair("QPSK 		193/1024", 0.3770), //
				eff_pair("QPSK 		308/1024", 0.6016), //
				eff_pair("QPSK 		449/1024", 0.8770), //
				eff_pair("QPSK 		602/1024", 1.1758), //
				eff_pair("QAM16 	378/1024", 1.4766), //
				eff_pair("QAM16 	490/1024", 1.9141), //
				eff_pair("QAM16 	616/1024", 2.4063), //
				eff_pair("QAM64 	466/1024", 2.7305), //
				eff_pair("QAM64		567/1024", 3.3223), //
				eff_pair("QAM64		666/1024", 3.9023), //
				eff_pair("QAM64		772/1024", 4.5234), //
				eff_pair("QAM64		873/1024", 5.1152), //
				eff_pair("QAM64		948/1024", 5.5547) };

		minSnr = { 1.95, 4.00, 6.00, 8.00, 10.00, 11.95, 14.05, 16.00, 17.90, 19.90, 21.50, 23.45, 25.00, 27.30, 29.00 };
		haveResources = false;
	}
	~RanPhy() {
	}

	double CalcEfficiency(double snr) {
		for (uint32_t i = minSnr.size() - 1; i > 0; i--) {
			assert(i < minSnr.size());
			if (minSnr.at(i) < snr) {
				haveResources = true;
				return efficiency.at(i).second;
			}
		}
		haveResources = false;
		return 0;
	}
	double GetMinEfficiency() {
		return efficiency.at(0).second;
	}
	double GetMaxEfficiency()
	{
		return efficiency.at(efficiency.size() - 1).second;
	}
	bool HaveResources() {
		return haveResources;
	}

private:

	std::vector<eff_pair> efficiency;
	std::vector<double> minSnr;
	bool haveResources;
};

#endif /* INCLUDE_PHY_PHY_H_ */
