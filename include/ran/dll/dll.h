/*
 * dll.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_DLL_DLL_H_
#define INCLUDE_DLL_DLL_H_

#include "qos.h"
#include "resources.h"
#include "access-plan.h"
#include "log.h"

class RanDll {
public:
	RanDll() {
		m_haveResource = true;
		m_rbsForLastSchedule = 0;
	}
	~RanDll() {

	}

	std::vector<ResourceBlockGroup> PlanResources(double bpcs, QoS qos, uint32_t t) {
		RM_FUNC_LOG(DLL_LOG);
		std::vector<ResourceBlockGroup> groups;
		m_haveResource = true;
		AccessPlan plan(bpcs, qos);
		plan.Make(t, m_rMap);
		if (plan.IsValid()) {
			groups = plan.GetGroups();
			m_rbsForLastSchedule = 0;
			for (auto group : groups) {
				m_rbsForLastSchedule += group.size();
			}

		} else {
			m_haveResource = false;
		}
		return groups;
	}

	void ReserveResources(std::vector<ResourceBlockGroup> groups) {
		RM_FUNC_LOG(DLL_LOG);
		for (auto group : groups) {
			m_rMap.Reserve(group);
		}
//		std::cout << m_rMap << std::endl;
	}

	double GetResourceUsage() {
		return m_rMap.GetReInUse();
	}

	bool HaveResources() {
		return m_haveResource;
	}

	uint32_t GetNumRbs() {
		return m_rbsForLastSchedule;
	}
	bool IsReservedAt(uint32_t t, uint32_t pos) {
		return m_rMap.IsReservedAt(t, pos);
	}

	CarriersUsage GetInterference(std::vector<ResourceBlockGroup> rbg) {
		RM_FUNC_LOG(DLL_LOG);
		ResourceMap rMap;
		for (auto g : rbg)
			rMap.Reserve(g);
		return m_rMap.GetInterference(rMap);
	}

	CarriersUsage GetCarriersUsage(std::vector<ResourceBlockGroup> rbg) {
		RM_FUNC_LOG(DLL_LOG);
		ResourceMap rMap;
		for (auto g : rbg)
			rMap.Reserve(g);
		return rMap.GetCarriersUsage();
	}
	void Reset() {
		m_haveResource = true;
		m_rbsForLastSchedule = 0;
		m_rMap = ResourceMap();
	}

	friend std::ostream& operator<<(std::ostream &os, RanDll &obj) {
		os << obj.m_rMap;
		return os;
	}

private:

	bool m_haveResource;
	uint32_t m_rbsForLastSchedule;
	ResourceMap m_rMap;
};

#endif /* INCLUDE_DLL_DLL_H_ */
