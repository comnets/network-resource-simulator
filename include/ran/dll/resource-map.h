/*
 * resource-map.h
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_DLL_RESOURCE_MAP_H_
#define INCLUDE_DLL_RESOURCE_MAP_H_

#include "resources.h"
#include "log.h"

#define SINGLE_MAP_TIME_SLOTS		20
#define REPEAT_TIME_SLOTS			4
#define NUM_TIME_SLOTS				(SINGLE_MAP_TIME_SLOTS * REPEAT_TIME_SLOTS)

class ResourceMap {

public:
	ResourceMap() {
		reMap = { // new period
				0, 0, 0, 1200, 1000, 1128, 1128, 940, 1128, 1128, 1128, 1000, 1200, 1200,
				// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200,
						// new period
						0, 0, 0, 1200, 1000, 1200, 1200, 1000, 1200, 1200, 1200, 1000, 1200, 1200 };

		for (uint32_t i = 0; i < REPEAT_TIME_SLOTS; i++) {
			uint32_t numCarriers = 0;
			uint32_t c = 0;
			for (auto n : reMap) {
				c++;
				numCarriers += n;
				if (c == ResourceBlock::numT) {
					resources.AddTimeSlot(floor((double) numCarriers / (double) ResourceBlock::numRe / (double) ResourceBlock::numT));
					c = 0;
					numCarriers = 0;
				}

			}
		}
		assert(resources.GetNumTimeSlots() == NUM_TIME_SLOTS);
	}

	uint32_t GetVacantAt(uint32_t t) {
		return resources.GetVacantAt(t);
	}

	uint32_t GetNumTimeSlots() {
		return resources.GetNumTimeSlots();
	}

	double GetReInUse() {
		return resources.GetInUseRatio();
	}

	uint32_t GetRes(ResourceBlockGroup rbg) {
		uint32_t carriers = 0;
		for (auto rb : rbg) {
			carriers += ResourceBlock::numRe * ResourceBlock::numT * rb.shareOfDataCarriers;
		}
		return carriers;
	}

	bool IsReservedAt(uint32_t t, uint32_t pos) {
		return resources.IsReservedAt(t, pos);
	}

	void Reserve(ResourceBlock rb, Resources &resources) {
		resources.AddReservedAt(rb.startT, rb.startRe);
	}

	void Reserve(ResourceBlockGroup rbg) {
		for (auto rb : rbg) {
			resources.AddReservedAt(rb.startT, rb.startRe);
		}
	}

	bool Match(ResourceBlock &rb, uint32_t t, Resources &resources) {

//		RM_LOG(DLL_LOG, "Look for resources for RB " << rb << " starting in time slot " << t);

		auto ts = resources.GetNumTimeSlots();

		assert(t < ts);

		uint32_t symbols = 0;
		uint32_t vacantIndex = 0;
		for (uint32_t i = 0; i < ts; i++) {
			//
			// ti is the time slot for the resource block start
			//
			uint32_t ti = (t + i >= ts) ? t + i - ts : t + i;

			uint32_t k = 0;
			uint32_t fj = 0;
			bool vacant = false;
			for (uint32_t j = 0; j < Carriers::maxRe; j++) {
				//
				// check if the carrier j is not occupied in ResourceBlock::numT time slots in a sequence
				//
				if (!resources.IsReservedAt(ti, j)) {
					fj = j;
					vacant = true;
					break;
				}
			}
			if (vacant) {
				rb.startT = ti;
				rb.startRe = fj;
				rb.shareOfDataCarriers = resources.GetDataToLoad(ti, fj);
				return true;
			} else {
				//
				// increment the start of the resource block and repeat
				//
			}
		}
		return false;
	}

	bool Match(ResourceBlock &rb, uint32_t t) {
		return Match(rb, t, resources);
	}
//
//	bool Match(ResourceBlockGroup &rbg, uint32_t t, uint32_t d = 0) {
//		auto resources = this->resources;
//		auto ts = resources.GetNumTimeSlots();
//
//		//
//		// is 'u' in range [t, t+ ResourceBlock::numT]
//		// u == t : using exactly the same time slots for both resource blocks
//		// u == t + ResourceBlock::numT : just after the resource block u
//		//
//		auto in_range = [&ts](uint32_t t, uint32_t u) {
//			for (uint32_t i = 0; i <= 1; i++) {
//				uint32_t tau = (t + i >= ts) ? t + i - ts : t + i;
//				if (u == tau) return true;
//			}
//			return false;
//		};
//
//		//
//		// u is supposed to be after t
//		//
//		auto distance = [&ts](uint32_t t, uint32_t u) {
//			return (u > t) ? u - t : ts - t + u;
//		};
//
//		uint32_t tau = 0;
//
//		for (uint32_t i = 0; i < rbg.size(); i++) {
//			//
//			// find a place for the i-th resource block
//			//
//			if (Match(rbg.at(i), t, resources)) {
//				Reserve(rbg.at(i), resources);
//			} else {
//				RM_LOG(DLL_LOG, "Found no place for the " << i << "-th RB");
//				RM_LOG(DLL_LOG, "Resources in use: " << resources);
//				return false;
//			}
//			//
//			// if it is not the first resource block
//			//
//			if (i != 0) {
//				//
//				// the start of this block should be not later than the end of the previous block
//				//
//				if (!in_range(tau, rbg.at(i).startT)) {
//					RM_LOG(DLL_LOG, "The " << i << "-th RB starts at " << rbg.at(i).startT << ", while previous RB started at " << tau);
//					d += distance(t, rbg.at(i).startT);
//					if (d >= ts) {
//						RM_LOG(DLL_LOG, "Resources in use: " << resources);
//						return false;
//					} else {
//						return Match(rbg, rbg.at(i).startT, d);
//					}
//				} else {
//					tau = rbg.at(i).startT;
//				}
//			} else {
//				tau = rbg.at(i).startT;
//			}
//		}
//		return true;
//	}

	bool Match(ResourceBlockGroup &rbg, uint32_t t, uint32_t minNumREs, uint32_t d = 0) {

		assert(minNumREs != 0);

		auto resources = this->resources;
		auto ts = resources.GetNumTimeSlots();
		rbg.clear();

		//
		// is 'u' in range [t, t+ ResourceBlock::numT]
		// u == t : using exactly the same time slots for both resource blocks
		// u == t + ResourceBlock::numT : just after the resource block u
		//
		auto in_range = [&ts](uint32_t t, uint32_t u) {
			for (uint32_t i = 0; i <= 1; i++) {
				uint32_t tau = (t + i >= ts) ? t + i - ts : t + i;
				if (u == tau) return true;
			}
			return false;
		};

		//
		// u is supposed to be after t
		//
		auto distance = [&ts](uint32_t t, uint32_t u) {
			return (u > t) ? u - t : ts - t + u;
		};

		uint32_t tau = 0;

		uint32_t numREs = 0;
		while (numREs < minNumREs) {
			ResourceBlock rb;
			if (Match(rb, t, resources)) {
				Reserve(rb, resources);
				rbg.push_back(rb);
			} else {
				RM_LOG(DLL_LOG, "Found no place for the " << rbg.size() << "-th RB");
				RM_LOG(DLL_LOG, "Resources in use: " << resources);
				return false;
			}
			//
			// if it is not the first resource block
			//
			if (rbg.size() > 1) {
				//
				// the start of this block should be not later than the end of the previous block
				//
				if (!in_range(tau, rb.startT)) {
					RM_LOG(DLL_LOG, "The " << (rbg.size() - 1) << "-th RB starts at " << rb.startT << ", while previous RB started at " << tau);
					d += distance(t, rb.startT);
					if (d >= ts) {
						RM_LOG(DLL_LOG, "Resources in use: " << resources);
						return false;
					} else {
						if (minNumREs == 0) {
							std::cout << "minNumREs " << minNumREs << std::endl;
						}
						return Match(rbg, rb.startT, minNumREs, d);
					}
				} else {
					tau = rb.startT;
				}
			} else {
				tau = rb.startT;
			}
			numREs = GetRes(rbg);
		}

		return true;
	}

	CarriersUsage GetInterference(ResourceMap &rMap) {
		RM_FUNC_LOG(DLL_LOG);
		CarriersUsage u;

		for (uint32_t j = 0; j < Carriers::maxRe; j++) {
			for (uint32_t i = 0; i < NUM_TIME_SLOTS; i++) {
				u.at(j) += (rMap.resources.IsReservedAt(i, j)) && (resources.IsReservedAt(i, j));
			}
			u.at(j) /= (double) NUM_TIME_SLOTS;
		}
		return u;
	}

	CarriersUsage GetCarriersUsage() {
		RM_FUNC_LOG(DLL_LOG);
		CarriersUsage u;

		for (uint32_t j = 0; j < Carriers::maxRe; j++) {
			for (uint32_t i = 0; i < NUM_TIME_SLOTS; i++) {
				u.at(j) += resources.IsReservedAt(i, j);
			}
			u.at(j) /= (double) NUM_TIME_SLOTS;
		}
		return u;
	}

	Resources GetResources() {
		return resources;
	}

	ResourceMap& operator=(const ResourceMap &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		resources = obj.resources;
		reMap = obj.reMap;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, ResourceMap &obj) {

		os << obj.resources;
		return os;
	}

private:

	std::array<uint32_t, 140> reMap;
	Resources resources;
};

#endif /* INCLUDE_DLL_RESOURCE_MAP_H_ */
