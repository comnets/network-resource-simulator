/*
 * access-plan.h
 *
 *  Created on: Feb 21, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_DLL_ACCESS_PLAN_H_
#define INCLUDE_DLL_ACCESS_PLAN_H_

#include <vector>
#include <cmath>

#include "resource-map.h"
#include "qos.h"
#include "phy.h"
#include "log.h"

class AccessPlan {

public:
	AccessPlan(double bpcs, QoS qos) {
		this->qos = qos;
		this->bpcs = bpcs;
		numBits = qos.messageSize * 8;
		iat = qos.messageSize * 8 / qos.datarate / 1024 / 1024;
		validPlan = true;
	}

	void Make(uint32_t t, ResourceMap rmap) {
		RM_FUNC_LOG(DLL_LOG);
		validPlan = false;
		groups.clear();
		auto ts = rmap.GetNumTimeSlots();
		assert(t < ts);
		//
		// plan several transmission blocks, as much as Tx window and IAT allow
		//
		auto tsSec = ts * OFDM_SYM_DURATION * ResourceBlock::numT;
		auto rbDuration = OFDM_SYM_DURATION * ResourceBlock::numT;
		auto iniT = t * rbDuration;
		for (double k = 0; k < tsSec && t < ts;) {
//			//
//			// calculate an initial approximation of the number of required resource blocks
//			//
//			auto numRbs = ceil(numBits / (ResourceBlock::numRe * ResourceBlock::numT * bpcs));
//			if (numRbs > 100) numRbs *= 1.34674;			//to accelerate computation
//			ResourceBlockGroup rbg;
//			rbg.resize(numRbs - 1);
//			//
//			// increase the number of the resource blocks iteratively dependent on available resources
//			//
//			do {
//				rbg.resize(rbg.size() + 1);
//				RM_LOG(DLL_LOG, "Looking for a place for a RB group of size " << rbg.size());
//				//
//				// find resources for one group
//				//
//				bool success = rmap.Match(rbg, t);
//				if (success) {
//					//
//					// one resource block group was successfully scheduled
//					//
//				} else {
//					//
//					// no resources available
//					//
//					RM_LOG(DLL_LOG, "No resources available");
//					return;
//				}
//				RM_LOG(DLL_LOG, "In map so far " << rmap.GetRes(rbg) * bpcs << ", wanted bits " << numBits);
//			} while (rmap.GetRes(rbg) * bpcs < numBits);
//
////			std::cout << "Adding group " << std::endl;
////			for(auto rb : rbg)
////			{
////				std::cout << "t " << rb.startT << ", f " << rb.startRe << std::endl;
////			}
			ResourceBlockGroup rbg;
			auto minNumREs = ceil((double) numBits / bpcs);
			if (minNumREs == 0) {
				std::cout << "numBits " << numBits << ", bpcs " << bpcs << std::endl;
			}
			bool success = rmap.Match(rbg, t, minNumREs);
			if (success) {
				//
				// one resource block group was successfully scheduled
				//
			} else {
				//
				// no resources available
				//
				RM_LOG(DLL_LOG, "No resources available");
				return;
			}

			if (IsLatencyValid(rbg, t, rmap)) {
				groups.push_back(rbg);
				rmap.Reserve(rbg);
				iniT += iat;
				k += iat;
				auto r = (iniT > tsSec) ? iniT - tsSec : iniT;
				t = ceil(r / rbDuration);

//				auto r = t * rbDuration + iat;
//				r = (r > tsSec) ? r - tsSec : r;
//				auto tprime = t;
//				t = ceil(r / rbDuration);
//				k += (t > tprime) ? (t - tprime) * rbDuration : (ts - tprime + t) * rbDuration;
				RM_LOG(DLL_LOG,
						"IAT: " << iat << ", elsapsed time " << k << " max time " << tsSec << ", new arrival time slot " << t << " max time slots " << ts);

			} else {

				//
				// latency violation
				//
				RM_LOG(DLL_LOG, "Latency violation");
				return;
			}
		}
		validPlan = true;
	}

	bool IsValid() {
		return validPlan;
	}

	std::vector<ResourceBlockGroup> GetGroups() {
		return groups;
	}

	bool IsLatencyValid(ResourceBlockGroup rbg, uint32_t t, ResourceMap rmap) {
		assert(!rbg.empty());

		auto transStart = rbg.at(0).startT;
		auto transEnd = rbg.at(rbg.size() - 1).startT + 1;
		auto ts = rmap.GetNumTimeSlots();
		auto bufferingDelay = ((t <= transStart) ? transStart - t : transStart + ts - t);
		bufferingDelay *= ResourceBlock::numT;
		auto frontEndDelay = ceil(FRONT_END_DELAY / OFDM_SYM_DURATION);
		auto transmissionDelay = (transEnd > transStart) ? transEnd - transStart : ts - transStart + transEnd;
		transmissionDelay *= ResourceBlock::numT;
		double delay = bufferingDelay + frontEndDelay + transmissionDelay;
		RM_LOG(DLL_LOG, "Transmission start " << transStart << ", transmission end " << transEnd);
		RM_LOG(DLL_LOG,
				"Buffering " << bufferingDelay * OFDM_SYM_DURATION << ", front end " << frontEndDelay * OFDM_SYM_DURATION << ", transmission " << transmissionDelay * OFDM_SYM_DURATION);
		RM_LOG(DLL_LOG, "Latency requirement " << qos.latency / 1000.0 << ", achievable latency " << (double) delay * OFDM_SYM_DURATION);

		return qos.latency / 1000.0 >= (double) delay * OFDM_SYM_DURATION;
	}

private:

	std::vector<ResourceBlockGroup> groups;
	QoS qos;
	double bpcs;
	bool validPlan;
	uint32_t numBits;
	double iat;
};

#endif /* INCLUDE_DLL_ACCESS_PLAN_H_ */
