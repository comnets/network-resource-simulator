/*
 * carriersSet.h
 *
 *  Created on: Feb 21, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_DLL_RESOURCES_H_
#define INCLUDE_DLL_RESOURCES_H_

#include <map>
#include <stdint.h>
#include <vector>
#include <array>
#include <math.h>
#include <cmath>
#include <assert.h>
#include <iostream>

struct ResourceBlock {

	ResourceBlock()
	{
		startRe = 0;
		startT = 0;
		shareOfDataCarriers = 0;
	}

	uint32_t startRe;
	uint32_t startT;
	double shareOfDataCarriers;
	static const uint32_t numRe = 12;
	static const uint32_t numT = 7;

	friend std::ostream& operator<<(std::ostream &os, const ResourceBlock &obj) {
		os << "[Re " << obj.startRe << ", T " << obj.startT << ", S " << obj.shareOfDataCarriers << "]";
		return os;
	}
};

typedef std::vector<ResourceBlock> ResourceBlockGroup;

/*
 * all carriersSet in one transmission slot
 */
struct Carriers {

public:

	enum Status {
		VACANT, RESERVED_DATA
	};
	Carriers(uint32_t n = 0) : status(maxRe, VACANT), vacant(maxRe) {

		assert(n <= maxRe);
		shareOfDataCarriers = (double) n / (double) maxRe;
	}

	void AddReserved(uint32_t pos) {
		assert(vacant >= 0);
		assert(pos < maxRe);
		assert(status.at(pos) != RESERVED_DATA);
		vacant--;
		status.at(pos) = RESERVED_DATA;
	}
	inline uint32_t GetVacant() {
		return vacant;
	}
	double GetDataToLoad(uint32_t pos) {
		assert(pos < maxRe);
		return status.at(pos) == VACANT ? shareOfDataCarriers : 0;
	}
	inline bool IsReservedAt(uint32_t pos) {
		assert(pos < maxRe);
		return status.at(pos) == RESERVED_DATA;
	}
	inline uint32_t GetReserved() {
		return maxRe - vacant;
	}
	void Release(uint32_t pos) {
		assert(pos < maxRe);
		if (status.at(pos) == RESERVED_DATA) {
			status.at(pos) = VACANT;
			vacant++;
		}
	}

	/*
	 * maximum number of carrier groups of ResourceBlock::numRe carriersSet fitting in one OFDM symbol
	 */
	static const uint32_t maxRe = 100;

	Carriers& operator=(const Carriers &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		status = obj.status;
		vacant = obj.vacant;
		shareOfDataCarriers = obj.shareOfDataCarriers;
		return *this;
	}

private:

	/*
	 * number of carrier groups of ResourceBlock::numRe carriersSet
	 */
	std::vector<Status> status;
	int16_t vacant;
	/*
	 * is equal 1 if no control information is sent in this OFDM symbol
	 */
	double shareOfDataCarriers;
};

struct Resources {
public:

	Resources() {
		reInUse = 0;
	}
	/*
	 * n is a number of vacant REs in this time slot
	 */
	void AddTimeSlot(uint32_t n) {
		carriersSet.push_back(Carriers(n));
	}
	/*
	 * reserve REs at time slot t and position pos
	 */
	void AddReservedAt(uint32_t t, uint32_t pos) {
		assert(t < carriersSet.size());
		carriersSet.at(t).AddReserved(pos);
		reInUse++;
	}
	/*
	 * can number of vacant REs in time slot t
	 */
	uint32_t GetVacantAt(uint32_t t) {
		assert(t < carriersSet.size());
		return carriersSet.at(t).GetVacant();
	}
	double GetDataToLoad(uint32_t t, uint32_t pos) {
		return carriersSet.at(t).GetDataToLoad(pos);
	}
	bool IsReservedAt(uint32_t t, uint32_t pos) {
		assert(t < carriersSet.size());
		return carriersSet.at(t).IsReservedAt(pos);
	}
	double GetInUseRatio() {
		return (double) reInUse / (double) (carriersSet.size() * Carriers::maxRe);
	}
	uint32_t GetNumTimeSlots() {
		return carriersSet.size();
	}

	Resources& operator=(const Resources &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		reInUse = obj.reInUse;
		carriersSet = obj.carriersSet;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, Resources &obj) {

		for (uint32_t i = 0; i < obj.carriersSet.size(); i++) {
			for (uint32_t j = 0; j < Carriers::maxRe; j++) {
				os << obj.carriersSet.at(i).IsReservedAt(j);
			}
			std::cout << std::endl;
		}
		return os;
	}

private:
	std::vector<Carriers> carriersSet;
	uint32_t reInUse;
};

typedef std::vector<double> CarriersUsage;

#endif /* INCLUDE_DLL_RESOURCES_H_ */
