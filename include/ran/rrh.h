/*
 * rrh.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_RRH_H_
#define INCLUDE_RRH_H_

#include <stdint.h>
#include <vector>

struct RRH {

	RRH(double radioCellSide) {
		this->radioCellSide = radioCellSide;
		id = 0;
		x = 0;
		y = 0;
	}

	RRH& operator=(const RRH &rrh) {
		// check for self-assignment
		if (&rrh == this) return *this;
		id = rrh.id;
		x = rrh.x;
		y = rrh.y;
		radioCellSide = rrh.radioCellSide;
		return *this;
	}

	uint16_t id;
	//position
	double x;
	double y;
	double radioCellSide;
};

struct RRHField {

	RRHField(uint16_t numRrhs, double cellSize) {

		double cellW = 2.0 * cellSize;
		double cellH = sqrt(3) * cellSize;
		//		uint16_t numCellX = floor((numRrhs - cellW / 2.0) / (cellW + cellSize)) + 1;
		//		uint16_t numCellY = ceil((numRrhs / cellH - 1) * 2 + 1);	// numRrhs = [ 1 + ( n - 1 ) / 2 ] * cellH;

		uint16_t numCellX = ceil(sqrt((double) numRrhs));
		uint16_t numCellY = ceil((double) numRrhs / (double) numCellX);

//				std::cout << "cell size " << cellW << "x" << cellH << ", number of cells " << numCellX << "x" << numCellY << std::endl;
		uint16_t c = 0;
		for (uint16_t i = 0; i < numCellY; i++) {
			for (uint16_t j = 0; j < numCellX; j++) {
				RRH rrh(cellSize);
				rrh.id = c++;
				rrh.x = 0.5 * cellW + j * (cellW + cellSize);
				rrh.y = (1 + (double) (i - 1) / 2) * cellH;
				if (((i + 1) & 1) == 0) rrh.x += cellW;
//				std::cout << "RRH " << rrh.id << " at " << rrh.x << ", " << rrh.y << std::endl;
				rrhs.push_back(rrh);
				if (rrhs.size() == numRrhs) break;
			}
			if (rrhs.size() == numRrhs) break;
		}
	}

	std::vector<RRH> rrhs;

};

#endif /* INCLUDE_RRH_H_ */
