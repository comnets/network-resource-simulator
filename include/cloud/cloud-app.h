/*
 * cloud-app.h
 *
 *  Created on: Feb 23, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CLOUD_CLOUD_APP_H_
#define INCLUDE_CLOUD_CLOUD_APP_H_

#include <vector>
#include <stdint.h>
#include <map>
#include <assert.h>
#include <iostream>

#include "cloud-edge.h"
#include "cloud-node.h"

/*
 * Micro-services can be connected in a chain only
 */
struct MicroService {

	MicroService() {
		this->appId = -1;
		this->msId = -1;
		this->nodeId = -1;
		this->connectToMsId = -1;
	}
	MicroService(uint32_t appId, uint32_t msId, uint32_t connectToMsId, NodeRequirement nodeRequirement, EdgeRequirement edgeRequirement) {
		this->appId = appId;
		this->msId = msId;
		this->nodeRequirement = nodeRequirement;
		this->edgeRequirement = edgeRequirement;
		this->connectToMsId = connectToMsId;
		this->nodeId = -1;
	}

	MicroService& operator=(const MicroService &obj) {
		// check for self-assignment
		if (&obj == this)
			return *this;
		appId = obj.appId;
		msId = obj.msId;
		nodeId = obj.nodeId;
		connectToMsId = obj.connectToMsId;
		nodeRequirement = obj.nodeRequirement;
		edgeRequirement = obj.edgeRequirement;
		return *this;
	}
	friend std::ostream& operator<<(std::ostream &os, const MicroService &obj) {
		os << "App ID " << obj.appId << " MS ID " << obj.msId << " Node ID " << obj.nodeId << " Next MS ID " << obj.connectToMsId << " Node Req "
				<< obj.nodeRequirement << " Edge Req " << obj.edgeRequirement;
		return os;
	}

	/*
	 * ID of an application that the micro-service belongs to
	 */
	uint32_t appId;
	/*
	 * ID of the micro-service
	 */
	uint32_t msId;
	/*
	 * ID of assigned node
	 */
	uint32_t nodeId;

	uint32_t connectToMsId;

	EdgeRequirement edgeRequirement;
	NodeRequirement nodeRequirement;
};

struct CloudApp {

	CloudApp() {
		appId = -1;
		firstMsId = -1;
		lastMsId = -1;
	}
	CloudApp(uint32_t appId) {
		this->appId = appId;
		firstMsId = -1;
		lastMsId = -1;
	}

	/*
	 * first micro-service is a fake micro-services that requires no computation power but serves to connect
	 * the chain of micro-services to an RRH
	 * it also serves as a point of connection for the chain of micro-services to form a circle
	 */
	bool IsAppComplete() {

		NodeRequirement nodeRequirement(0, 0, 0);
		if (nodeRequirement >= ms[firstMsId].nodeRequirement) {
			if (ms[lastMsId].connectToMsId == firstMsId) {
				return true;
			}
		}
		return false;
	}

	void AddFirstMicroService(uint32_t msIdFrom, uint32_t msIdTo, EdgeRequirement edgeRequirement) {
		assert(ms.empty());
		firstMsId = msIdFrom;
		lastMsId = msIdFrom;
		ms[msIdFrom] = MicroService(appId, msIdFrom, msIdTo, NodeRequirement(0, 0, 0), edgeRequirement);
	}

	void AddMicroService(uint32_t msIdFrom, uint32_t msIdTo, NodeRequirement nodeRequirement, EdgeRequirement edgeRequirement) {
		assert(!ms.empty());
		assert(ms.find(msIdFrom) == ms.end());
		lastMsId = msIdFrom;
		ms[msIdFrom] = MicroService(appId, msIdFrom, msIdTo, nodeRequirement, edgeRequirement);
	}

	void AddLastMicroService(uint32_t msIdFrom, NodeRequirement nodeRequirement, EdgeRequirement edgeRequirement) {
		assert(!ms.empty());
		assert(ms.find(msIdFrom) == ms.end());
		lastMsId = msIdFrom;
		ms[msIdFrom] = MicroService(appId, msIdFrom, firstMsId, nodeRequirement, edgeRequirement);
	}

	NodeRequirement GetCumulativeNodeRequirement(uint32_t firstMsId, uint32_t lastMsId) {
		NodeRequirement requirement;

		for (uint32_t i = firstMsId; i <= lastMsId; i++) {
			requirement += ms[i].nodeRequirement;
		}

		return requirement;
	}

	CloudApp& operator=(const CloudApp &obj) {
		// check for self-assignment
		if (&obj == this)
			return *this;
		appId = obj.appId;
		firstMsId = obj.firstMsId;
		lastMsId = obj.lastMsId;
		ms = obj.ms;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, const CloudApp &obj) {
		for (uint32_t i = obj.firstMsId; i <= obj.lastMsId; i++) {
			os << std::endl << "MicroService [" << obj.ms.at(i) << "]";
		}
		return os;
	}

	uint32_t firstMsId;
	uint32_t lastMsId;
	uint32_t appId;
	std::map<uint32_t, MicroService> ms;
};

#endif /* INCLUDE_CLOUD_CLOUD_APP_H_ */
