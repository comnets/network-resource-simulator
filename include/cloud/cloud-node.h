/*
 * cloud-node.h
 *
 *  Created on: Feb 23, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CLOUD_CLOUD_NODE_H_
#define INCLUDE_CLOUD_CLOUD_NODE_H_

#include <stdint.h>
#include <map>
#include <algorithm>
#include <random>
#include <iostream>
#include <chrono>

#include "qos.h"

#define UNDEF_VAL	65000

struct NodeResource {

	NodeResource() {
		cpu = 0;
		ram = 0;
		flash = 0;
	}
	NodeResource(double cpu, double ram, double flash) {
		this->cpu = cpu;
		this->ram = ram;
		this->flash = flash;
	}

	NodeResource& operator=(const NodeResource &obj) {
		// check for self-assignment
		if (&obj == this)
			return *this;
		cpu = obj.cpu;
		ram = obj.ram;
		flash = obj.flash;
		return *this;
	}

	NodeResource& operator+=(const NodeResource &rhs) {
		this->cpu += rhs.cpu;
		this->ram += rhs.ram;
		this->flash += rhs.flash;
		return *this;
	}

	friend NodeResource operator-(NodeResource lhs, const NodeResource &rhs) {
		lhs.cpu -= rhs.cpu;
		lhs.ram -= rhs.ram;
		lhs.flash -= rhs.flash;
		return lhs;
	}

	inline friend bool operator>=(const NodeResource &lhs, const NodeResource &rhs) {
		return !(lhs.cpu < rhs.cpu || lhs.ram < rhs.ram || lhs.flash < rhs.flash);
	}

	friend std::ostream& operator<<(std::ostream &os, const NodeResource &obj) {
		os << "[ CPU: " << obj.cpu << ", RAM: " << obj.ram << ", Flash: " << obj.flash << "]";
		return os;
	}

	double cpu;
	double ram;
	double flash;
};

typedef NodeResource NodeRequirement;

struct CloudNode {

	CloudNode() {
		this->id = UNDEF_VAL;
		this->rrhId = UNDEF_VAL;
		generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
	}
	CloudNode(uint32_t id, NodeResource capacity, Performance performance) {
		this->id = id;
		this->capacity = capacity;
		this->performance = performance;
		this->rrhId = UNDEF_VAL;
		generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
		UpdatePerformance();
	}
	void ConnectToRrh(uint32_t rrhId) {
		this->rrhId = rrhId;
	}

	void AddNeighbor(uint32_t neighborId) {
		if (std::find(neighbors.begin(), neighbors.end(), neighborId) != neighbors.end())
			return;
		neighbors.push_back(neighborId);
	}

	bool CanAccomodate(NodeRequirement requirement) {
		assert(capacity >= inuse);
		auto c = capacity - inuse;
		return (capacity - inuse) >= requirement;
	}

	void Assign(NodeRequirement requirement) {
		inuse += requirement;
		assert(capacity >= inuse);
		UpdatePerformance();
	}
	uint32_t GetRandomNeighbor() {
		std::uniform_int_distribution<uint32_t> distribution(0, neighbors.size() - 1);
		return neighbors.at(distribution(generator));
	}
	NodeResource GetUsage() {
		return inuse;
	}
	double GetUsageRatio() {
		auto useCpu = inuse.cpu / capacity.cpu;
		auto useRam = inuse.ram / capacity.ram;
		auto useFlash = inuse.flash / capacity.flash;

		return std::max(std::max(useCpu, useRam), useFlash);
	}

	std::vector<uint32_t> GetNeighbors() {
		return neighbors;
	}
	CloudNode& operator=(const CloudNode &obj) {
		// check for self-assignment
		if (&obj == this)
			return *this;
		id = obj.id;
		capacity = obj.capacity;
		inuse = obj.inuse;
		neighbors = obj.neighbors;
		performance = obj.performance;
		rrhId = obj.rrhId;
		return *this;
	}

	void UpdatePerformance() {

	}

	void Reset()
	{
		inuse.cpu = 0;
		inuse.ram = 0;
		inuse.flash = 0;
		UpdatePerformance();
	}

	uint32_t id;
	NodeResource capacity;
	NodeResource inuse;
	std::vector<uint32_t> neighbors;
	Performance performance;
	uint32_t rrhId;
	std::default_random_engine generator;

};

#endif /* INCLUDE_CLOUD_CLOUD_NODE_H_ */
