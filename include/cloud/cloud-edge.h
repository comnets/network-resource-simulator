/*
 * cloud-edge.h
 *
 *  Created on: Feb 23, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CLOUD_CLOUD_EDGE_H_
#define INCLUDE_CLOUD_CLOUD_EDGE_H_

#include <stdint.h>
#include <iostream>
#include <vector>

#include "qos.h"

struct EdgeResource {
	EdgeResource() {
		bandwidth = 0;
	}
	EdgeResource(double bandwidth) {
		this->bandwidth = bandwidth;
	}

	EdgeResource& operator=(const EdgeResource &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		bandwidth = obj.bandwidth;
		return *this;
	}

	friend EdgeResource operator+(EdgeResource lhs, const EdgeResource &rhs) {
		lhs.bandwidth += rhs.bandwidth;
		return lhs;
	}

	friend EdgeResource operator-(EdgeResource lhs, const EdgeResource &rhs) {
		lhs.bandwidth -= rhs.bandwidth;
		return lhs;
	}

	inline friend bool operator>=(const EdgeResource &lhs, const EdgeResource &rhs) {
		return !(lhs.bandwidth < rhs.bandwidth);
	}
	friend std::ostream& operator<<(std::ostream &os, const EdgeResource &obj) {
		os << "[ Bandwidth " << obj.bandwidth << "]";
		return os;
	}

	double bandwidth;
};

typedef EdgeResource EdgeRequirement;

struct CloudEdge {

	CloudEdge() {
		this->node1 = -1;
		this->node2 = -1;
	}

	CloudEdge(uint32_t node1, uint32_t node2, EdgeResource capacity, Performance performance) {
		this->node1 = node1;
		this->node2 = node2;
		this->capacity = capacity;
		this->performance = performance;
		UpdatePerformance();
	}

	CloudEdge(const CloudEdge &cloudEdge) {
		node1 = cloudEdge.node1;
		node2 = cloudEdge.node2;
		capacity = cloudEdge.capacity;
		performance = cloudEdge.performance;
		UpdatePerformance();
	}

	bool CanAccomodate(EdgeRequirement requirement) {
		assert(capacity >= inuse);
		return (capacity - inuse) >= requirement;
	}
	void Assign(EdgeRequirement requirement) {
//		if (!(capacity >= inuse + requirement))
//			std::cout << "Capacity " << capacity << " inuse " << inuse << " requirement " << requirement << " can accomodate " << CanAccomodate(requirement)
//					<< std::endl;
		inuse = inuse + requirement;

		assert(capacity >= inuse);
		UpdatePerformance();
	}

	double GetUsageRatio() {
		return inuse.bandwidth / capacity.bandwidth;
	}

	CloudEdge& operator=(const CloudEdge &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		node1 = obj.node1;
		node2 = obj.node2;
		capacity = obj.capacity;
		inuse = obj.inuse;
		performance = obj.performance;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &os, const CloudEdge &obj) {
		os << "[" << obj.node1 << ", " << obj.node2 << "]";
		return os;
	}

	void UpdatePerformance() {
		performance.datarate = capacity.bandwidth - inuse.bandwidth;
	}

	void Reset() {
		inuse.bandwidth = 0;
		UpdatePerformance();
	}
	/*
	 * end nodes (IDs)
	 */
	uint32_t node1;
	uint32_t node2;
	EdgeResource capacity;
	EdgeResource inuse;
	Performance performance;
};

struct HyperEdge {
public:
	HyperEdge() {

	}

	void Add(CloudEdge edge) {
		edges.push_back(edge);
//		std::cout << "Added edge " << edge << " with perf. " << edge.performance << " to hyper-edge. Actual number of edges: " << edges.size() << std::endl;
		performance = performance + edge.performance;
	}

	bool IsEmpty() {
		return edges.empty();
	}

	bool CanAccomodate(EdgeRequirement requirement) {
		requirement = ConvertToHyperEdgeReq(requirement);
		for (auto &edge : edges) {
			if (!edge.CanAccomodate(requirement)) {
				return false;
			}
		}
		return true;
	}

	Performance GetPerformance() {
		return performance;
	}

	HyperEdge& operator=(const HyperEdge &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		edges = obj.edges;
		performance = obj.performance;
		return *this;
	}
	void Reset() {
		edges.clear();
		performance = Performance();
	}

	std::vector<CloudEdge> GetEdges() {
		return edges;
	}

	friend std::ostream& operator<<(std::ostream &os, HyperEdge &obj) {
		for (auto edge : obj.edges) {
			os << "Edge (" << edge.node1 << "," << edge.node2 << ")" << std::endl;
		}
		return os;
	}

	EdgeRequirement ConvertToHyperEdgeReq(EdgeRequirement req) {
		req.bandwidth *= edges.size();
		return req;
	}

private:

	std::vector<CloudEdge> edges;
	Performance performance;
};

struct EdgeContainer {

public:
	bool Have(uint32_t node1, uint32_t node2) {

		for (auto &edge : edges) {
			if ((edge.node1 == node1 && edge.node2 == node2) || (edge.node1 == node2 && edge.node2 == node1)) return true;
		}
		return false;
	}
	bool Have(uint32_t node) {

		for (auto &edge : edges) {
			if (edge.node1 == node || edge.node2 == node) return true;
		}
		return false;
	}
	CloudEdge& Get(uint32_t node1, uint32_t node2) {

		for (auto &edge : edges) {
			if ((edge.node1 == node1 && edge.node2 == node2) || (edge.node1 == node2 && edge.node2 == node1)) return edge;
		}
		assert(0 && "Found no edge");
		return placeHolder;
	}

	void Assign(uint32_t node1, uint32_t node2, EdgeRequirement requirement) {

		for (uint32_t i = 0; i < edges.size(); i++) {
			if ((edges.at(i).node1 == node1 && edges.at(i).node2 == node2) || (edges.at(i).node1 == node2 && edges.at(i).node2 == node1)) {
				edges.at(i).Assign(requirement);
			}
		}
	}

	void Assign(HyperEdge hEdge, EdgeRequirement requirement) {

		requirement = hEdge.ConvertToHyperEdgeReq(requirement);

		auto edges = hEdge.GetEdges();
		for (auto edge : edges) {
			this->Assign(edge.node1, edge.node2, requirement);
		}

	}

	void Add(CloudEdge edge) {
		edges.push_back(edge);
	}
	EdgeContainer& operator=(const EdgeContainer &obj) {
		// check for self-assignment
		if (&obj == this) return *this;
		edges = obj.edges;
		return *this;
	}
	void Reset() {
		edges.clear();
	}

	friend std::ostream& operator<<(std::ostream &os, EdgeContainer &obj) {
		for (uint32_t i = 0; i < obj.edges.size(); i++) {
			os << "Edge (" << obj.edges.at(i).node1 << "," << obj.edges.at(i).node2 << "): in use " << obj.edges.at(i).inuse << std::endl;
		}

		return os;
	}

private:
	std::vector<CloudEdge> edges;
	CloudEdge placeHolder;
};

#endif /* INCLUDE_CLOUD_CLOUD_EDGE_H_ */
