/*
 * cloud.h
 *
 *  Created on: Feb 23, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CLOUD_CLOUD_H_
#define INCLUDE_CLOUD_CLOUD_H_

#include <map>
#include <stdint.h>
#include <assert.h>
#include <memory>
#include <vector>
#include <functional>
#include <limits>
#include <chrono>

#include "cloud-app.h"
#include "log.h"
#include "eval-profile.h"

struct Cloud {

public:
	Cloud(EvalProfile eval) {
		nodePerformance = eval.nodePerformance;
		edgePerformance = eval.edgePerformance;
		maxCloudUsefulness = 0;
	}

	void AddNode(uint32_t nodeId, NodeResource nodeCapacity) {
		nodes[nodeId] = CloudNode(nodeId, nodeCapacity, nodePerformance);
	}

	void Connect(uint32_t nodeIdFrom, uint32_t nodeIdTo, EdgeResource edgeCapacity) {
		assert(nodes.find(nodeIdFrom) != nodes.end());
		auto edge = CloudEdge(nodeIdFrom, nodeIdTo, edgeCapacity, edgePerformance);
		/*
		 * consider a bi-directional channel
		 */
		edges.Add(edge);
		nodes[nodeIdFrom].AddNeighbor(nodeIdTo);
		nodes[nodeIdTo].AddNeighbor(nodeIdFrom);
	}
	void ConnectRrh(uint32_t nodeId, uint32_t rrhId) {
		assert(nodes.find(nodeId) != nodes.end());
		nodes[nodeId].ConnectToRrh(rrhId);
	}
	bool HasRrhConnected(uint32_t nodeId) {
		return nodes[nodeId].rrhId != UNDEF_VAL;
	}
	bool HasNodeConnected(uint32_t nodeId1, uint32_t nodeId2) {
		return edges.Have(nodeId1, nodeId2);
	}

	/*
	 * returns true if the node has at least one neighbor
	 */
	bool IsNodeConnected(uint32_t id) {
		return edges.Have(id);
	}
	/*
	 * returns true if the RRH is assigned to a node
	 */
	bool IsRrhConnected(uint32_t id) {
		for (auto x : nodes) {
			if (x.second.rrhId == id) return true;
		}
		return false;
	}
	NodeResource GetNodeUsage(uint32_t id) {
		return nodes[id].GetUsage();
	}

	bool DeployApp(CloudApp &app, QoS qos, uint32_t rrhId) {

		//
		// deploy only if the deployment is feasible
		//
		auto nodes = this->nodes;
		auto edges = this->edges;

		auto success = DoDeployApp(nodes, edges, app, qos, rrhId);

		if (success) {
			this->nodes = nodes;
			this->edges = edges;
			return true;
		} else {
			return false;
		}
	}

	double GetNodeUsefulness(CloudNode node) {
		auto neighbors = node.GetNeighbors();
		double usefulness = 1 - node.GetUsageRatio();
		for (auto neighbor : neighbors) {
			auto edgeUsage = this->edges.Get(node.id, neighbor).GetUsageRatio();
			usefulness += (1 - edgeUsage) * (1 - this->nodes.at(neighbor).GetUsageRatio());
		}
		return usefulness;
	}

	double GetResourceUsage() {
		double usefulness = 0;
		for (auto node : nodes) {
			usefulness += GetNodeUsefulness(node.second);
		}
		return (maxCloudUsefulness - usefulness) / maxCloudUsefulness;
	}

	void CalcMaxCloudResources() {
		double usefulness = 0;
		for (auto node : nodes) {
			usefulness += GetNodeUsefulness(node.second);
		}
		maxCloudUsefulness = usefulness;
	}

	void Reset() {

		nodes.clear();
		edges.Reset();
	}

	friend std::ostream& operator<<(std::ostream &os, Cloud &obj) {
		for (auto node : obj.nodes) {
			os << "Node " << node.first << ": in use " << node.second.inuse << std::endl;
		}
		os << obj.edges << std::endl;
		return os;
	}

private:

	bool DoDeployApp(std::map<uint32_t, CloudNode> &nodes, EdgeContainer &edges, CloudApp &app, QoS targetQos, uint32_t rrhId) {

		auto nodeId = GetNodeIdConnectedToRrh(rrhId);
		QoS achievedQos(0, std::numeric_limits<double>::max(), 0, 0);
		auto firstMsId = app.firstMsId;
		auto msId = app.lastMsId;
		std::map<uint32_t, CloudNode> inspectedNodes;
		HyperEdge hEdge;
		inspectedNodes[nodeId] = nodes[nodeId];

		for (; msId >= firstMsId;) {

			//
			// calculate a node requirement for a set of micro-services
			//
			auto nodeRequirement = app.GetCumulativeNodeRequirement(firstMsId, msId);
			RM_LOG(CLOUD_LOG, "Cumulative Req for MS [" << firstMsId << "," << msId << "] -> " << nodeRequirement);
			//
			// check if the current node can accommodate the set of micro-services
			//
			auto componentQosNode = QoS(0, std::numeric_limits<double>::max(), nodes[nodeId].performance.latency, nodes[nodeId].performance.jitter);
			auto newQos = achievedQos + componentQosNode * (msId - firstMsId + (firstMsId == app.firstMsId ? 0 : 1));
			RM_LOG(CLOUD_LOG, "Target QoS " << targetQos);
			RM_LOG(CLOUD_LOG, "So far achieved QoS " << achievedQos);
			RM_LOG(CLOUD_LOG, "New achievable QoS " << newQos << " summed up for " << (msId - firstMsId + (firstMsId == app.firstMsId ? 0 : 1)) << " MS");
			if (nodes[nodeId].CanAccomodate(nodeRequirement) && !newQos.Violate(targetQos)) {
				RM_LOG(CLOUD_LOG, "Node " << nodeId << " can fulfill the requirement");
				//
				// assign the set of micro-services to this node
				// note that this node will have no resources remaining if this set is not full
				//
				for (auto msId2 = firstMsId; msId2 <= msId; msId2++) {
					app.ms[msId2].nodeId = nodeId;
					nodes[nodeId].Assign(app.ms[msId2].nodeRequirement);
					RM_LOG(CLOUD_LOG, "Assign MS " << msId2 << " to node " << nodeId);
				}
				achievedQos = newQos;
				if (msId == app.lastMsId) {
					//
					// all micro-services has been assigned
					//
					RM_LOG(CLOUD_LOG, "All micro-services were assigned to nodes");
					return true;
				}
				//
				// find a new node candidate
				//
				RM_LOG(CLOUD_LOG, "MS [" << msId + 1 << "," << app.lastMsId << "] are not assigned yet");
				if (!FindNewCandidate(targetQos, achievedQos, nodeId, app, msId, nodes, edges, inspectedNodes, hEdge)) {
					//
					// no edge resources in all out-coming edges or node resources of all egress nodes are available
					//
					return false;
				}
				//
				// continue the loop over the remaining set of micro-services
				//
				firstMsId = msId + 1;
				msId = app.lastMsId;
			} else {

				RM_LOG(CLOUD_LOG, "Node " << nodeId << " cannot fulfill the requirement");
				if (msId == firstMsId) {

					//
					// find a new node candidate
					//
					RM_LOG(CLOUD_LOG, "Node " << nodeId << " cannot fulfill any requirement, look for another node");
					RM_LOG(CLOUD_LOG, "MS [" << msId + 1 << "," << app.lastMsId << "] are not assigned yet");
//					assert(0);
					if (!FindNewCandidate(targetQos, achievedQos, nodeId, app, msId, nodes, edges, inspectedNodes, hEdge)) {
						//
						// no edge resources in all out-coming edges or no node resources of all egress nodes are available
						//
						return false;
					}
					//
					// start again for the complete remaining set of unassigned micro-services
					//
					msId = app.lastMsId;
				} else {
					//
					// if the node cannot accommodate the set of micro-services then reduce the set
					//
					msId--;
				}
			}
		}

		return false;
	}

	bool FindNewCandidate(QoS targetQos, QoS &achievedQos, uint32_t &nodeId, CloudApp app, uint32_t msId, std::map<uint32_t, CloudNode> &nodes,
			EdgeContainer &edges, std::map<uint32_t, CloudNode> &inspectedNodes, HyperEdge &hEdge) {

		//
		// Look of an node that can host at least one MS
		//
		RM_LOG(CLOUD_LOG, "Inspecting neighbors of node " << nodeId);
		auto neighbors = nodes[nodeId].neighbors;
		uint32_t candidateRepeaterId = UNDEF_VAL;
		std::default_random_engine generator;
		generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
		uint32_t neighborsNum = neighbors.size();
		bool found = false;
		for (uint32_t i = 0; i < neighborsNum; i++) {

			//
			// get new node ID
			//
			std::uniform_int_distribution<uint32_t> distribution(0, neighbors.size() - 1);
			auto j = distribution(generator);
			auto newNodeId = neighbors.at(j);
			neighbors.erase(neighbors.begin() + j, neighbors.begin() + j + 1);
			if (inspectedNodes.find(newNodeId) != inspectedNodes.end()) {
				RM_LOG(CLOUD_LOG, "A random neighbor node " << newNodeId << " has been already inspected");
				continue;
			}
			RM_LOG(CLOUD_LOG, "Pick a random neighbor node " << newNodeId);
			inspectedNodes[newNodeId] = nodes[newNodeId];

			//
			// check out if the new node is the right candidate:
			// I. if the edge connecting this and previous nodes have sufficient capacity
			//
			HyperEdge tempHyperEdge = hEdge;
			auto edgeRequirement = app.ms[msId].edgeRequirement;
			tempHyperEdge.Add(edges.Get(nodeId, newNodeId));

			if (tempHyperEdge.CanAccomodate(edgeRequirement)) {
				RM_LOG(CLOUD_LOG, "Edge [" << nodeId << "," << newNodeId << "] fulfills capacity requirements of MS " << msId + 1 << ": " << edgeRequirement);
			} else {
				continue;
			}

			//
			// save a candidate repeater node for later
			//
			auto componentQosEdge = QoS(0, tempHyperEdge.GetPerformance().datarate, tempHyperEdge.GetPerformance().latency,
					tempHyperEdge.GetPerformance().jitter);
			auto newQos = achievedQos + componentQosEdge;
			RM_LOG(CLOUD_LOG, "Target QoS " << targetQos);
			RM_LOG(CLOUD_LOG, "So far achieved QoS " << achievedQos);
			RM_LOG(CLOUD_LOG, "New achievable QoS " << newQos);
			if (!newQos.Violate(targetQos)) {
				RM_LOG(CLOUD_LOG, "Edge [" << nodeId << "," << newNodeId << "] fulfills QoS requirements of MS " << msId + 1);
				candidateRepeaterId = newNodeId;
			} else {
				continue;
			}
			//
			// check out if the new node is the right candidate:
			// II. if the new node can accommodate at least one micro-service
			//
			if (nodes[newNodeId].CanAccomodate(app.ms[msId + 1].nodeRequirement)) {
				RM_LOG(CLOUD_LOG, "Node " << newNodeId << " can accommodate MS " << msId + 1 << " with requirements " << app.ms[msId + 1].nodeRequirement);
			} else {
				continue;
			}

			//
			// check out if the new node is the right candidate:
			// III. if QoS can be guaranteed for at least one micro-service
			//
			auto componentQosNode = QoS(0, std::numeric_limits<double>::max(), nodes[newNodeId].performance.latency, nodes[newNodeId].performance.jitter);
			newQos = achievedQos + componentQosNode + componentQosEdge;
			RM_LOG(CLOUD_LOG, "Target QoS " << targetQos);
			RM_LOG(CLOUD_LOG, "So far achieved QoS " << achievedQos);
			RM_LOG(CLOUD_LOG, "New achievable QoS " << newQos);

			if (!newQos.Violate(targetQos)) {
				achievedQos = achievedQos + componentQosEdge;
				RM_LOG(CLOUD_LOG, "QoS can be guaranteed");
				hEdge = tempHyperEdge;
				edges.Assign(hEdge, edgeRequirement);
				nodeId = newNodeId;
				found = true;
				break;
			} else {
				RM_LOG(CLOUD_LOG, "QoS cannot be guaranteed");
			}
		}

		//
		// check if a node can be selected as a repeater
		//
		if (!found) {

			if (candidateRepeaterId != UNDEF_VAL) {
				RM_LOG(CLOUD_LOG,
						"No neighbor of node " << nodeId << " can accommodate MS " << msId + 1 << " with requirements " << app.ms[msId + 1].nodeRequirement);
				RM_LOG(CLOUD_LOG, "Select node " << candidateRepeaterId << " as a repeater");

				hEdge.Add(edges.Get(nodeId, candidateRepeaterId));
				nodeId = candidateRepeaterId;
				FindNewCandidate(targetQos, achievedQos, nodeId, app, msId, nodes, edges, inspectedNodes, hEdge);

			} else {
				RM_LOG(CLOUD_LOG, "No neighbor cannot both host an MS and act as a repeater");
				return false;
			}
		}
		return true;
	}

	uint32_t GetNodeIdConnectedToRrh(uint32_t rrhId) {
		for (auto const &x : nodes) {
			if (x.second.rrhId == rrhId) return x.first;
		}
		assert(0 && "No node is connected to the specified RRH");
		return -1;
	}

	std::map<uint32_t, CloudNode> nodes;
	EdgeContainer edges;
	Performance nodePerformance;
	Performance edgePerformance;
	double maxCloudUsefulness;
};

#endif /* INCLUDE_CLOUD_CLOUD_H_ */
