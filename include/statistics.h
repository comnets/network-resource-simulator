/*
 * statistics.h
 *
 *  Created on: Mar 19, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_STATISTICS_H_
#define INCLUDE_STATISTICS_H_

#include <boost/math/distributions/students_t.hpp>
#include <iostream>
#include <vector>
#include <numeric>
#include <random>
#include <stdint.h>
#include <assert.h>

#define NUM_CONF_INTERV_SAMPLES 20
#define PROB_CONF_INTERVAL 0.95

struct Statistics {
public:

	Statistics() {
		mean = 0;
		stdev = 0;
	}
	Statistics Calc(std::vector<double> v) {
		CalcMean(v);
		CalcStdev(v);
		return *this;
	}
	Statistics Calc(std::vector<uint32_t> vint) {
		std::vector<double> v(vint.begin(), vint.end());
		CalcMean(v);
		CalcStdev(v);
		return *this;
	}
	Statistics& operator=(const Statistics &obj) {
		if (&obj == this) return *this;
		mean = obj.mean;
		stdev = obj.stdev;
		return *this;
	}
	friend std::ostream& operator<<(std::ostream &os, const Statistics &obj) {
		os << obj.mean << "\t" << obj.stdev;
		return os;
	}

	Statistics CalcCI(std::vector<double> samples) {
		double confInterval = 0;
		double accuracyIndex = 1;
		assert(samples.size () == NUM_CONF_INTERV_SAMPLES);
		assert(PROB_CONF_INTERVAL <= 1 && PROB_CONF_INTERVAL >= 0.5);

		CalcMean(samples);
		CalcStdev(samples);
		if(mean == 0 || stdev == 0) return *this;

		const double alpha = 1 - PROB_CONF_INTERVAL;
		const boost::math::students_t dist(NUM_CONF_INTERV_SAMPLES - 1);
		using boost::math::quantile;
		using boost::math::complement;
		double T = quantile(complement(dist, alpha / 2));
		RM_LOG(STATS_LOG, "T-Student coefficient: " << T);
		//
		// Calculate width of interval (one sided)
		//
		confInterval = T * stdev / sqrt(double(NUM_CONF_INTERV_SAMPLES));
		RM_LOG(STATS_LOG, "One-sided confidence interval: " << confInterval);

		accuracyIndex = 1 - confInterval / mean;
		RM_LOG(STATS_LOG, "Accuracy index: " << accuracyIndex);

		stdev = confInterval;
		return *this;
	}
	Statistics CalcCI(std::vector<uint32_t> vint) {
		std::vector<double> v(vint.begin(), vint.end());
		return CalcCI(v);
	}

private:

	void CalcMean(std::vector<double> v) {
		double sum = std::accumulate(v.begin(), v.end(), 0.0);
		mean = sum / v.size();
	}
	void CalcStdev(std::vector<double> v) {
		double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
		stdev = std::sqrt(sq_sum / v.size() - mean * mean);
	}

	double mean;
	double stdev;
};

#endif /* INCLUDE_STATISTICS_H_ */
