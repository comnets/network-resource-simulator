/*
 * simulation.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_SIMULATION_H_
#define INCLUDE_SIMULATION_H_

#include <stdint.h>
#include <iostream>
#include <random>

#include "mdmo.h"
#include "ue-manager.h"
#include "eval-profile.h"
#include "statistics.h"

class Simulation {
public:
	Simulation(EvalProfile eval) : m_mdmo(eval), m_ueManager(eval) {

		m_eval = eval;
		m_numJoinedAgvs = 0;
	}
	~Simulation() {

	}

	void Run() {

		m_numJoinedAgvs = 0;
		uint32_t numTriedToJoin = 0;

		while (!m_ueManager.HaveAllTriedToJoin()) {
			//
			// UE creates a request to MDMO for joining the network
			//
			auto reqMdmo = m_ueManager.CreateRequest();
			//
			// MDMO creates a response to UE
			//
			auto respMdmo = m_mdmo.HandleRequest(reqMdmo);

			if (respMdmo.success) {
				m_numJoinedAgvs++;
			}
			numTriedToJoin++;
			auto pa = (double) m_numJoinedAgvs / (double) numTriedToJoin;
			if (pa < m_eval.admissionP) break;
		}
	}

	void RunInterative(uint32_t numIter) {
		std::vector<uint32_t> numJoinedAgvs;
		std::vector<double> cloudUsage;
		std::vector<double> ranUsage;

		for (uint32_t i = 0; i < numIter; i++) {
			Run();
			numJoinedAgvs.push_back(m_numJoinedAgvs);
			cloudUsage.push_back(m_mdmo.GetCloudResourceUsage());
			ranUsage.push_back(m_mdmo.GetRanResourceUsage());
//			std::cout << m_numJoinedAgvs << "\t" << m_mdmo.GetCloudResourceUsage() << "\t" << m_mdmo.GetRanResourceUsage() << "\t"
//					<< m_mdmo.GetStatusStatistics() << std::endl;
			Reset();
//			std::cout << "Finished " << ((double) (i + 1) / (double) numIter * 100) << "%" << std::endl;
		}
		Statistics st;
		auto numJoinedAgvsStat = st.CalcCI(numJoinedAgvs);
		auto cloudUsageStat = st.CalcCI(cloudUsage);
		auto ranUsageStat = st.CalcCI(ranUsage);
		std::cout << m_eval << "\t" << numJoinedAgvsStat << "\t" << cloudUsageStat << "\t" << ranUsageStat << std::endl;
	}

	void Reset() {
		m_numJoinedAgvs = 0;
		m_mdmo.Reset();
		m_ueManager.Reset();
	}

	void PrintSummary() {
		std::cout << "Evaluation parameters " << m_eval << std::endl;
		std::cout << "Number of joined AGVs " << m_numJoinedAgvs << std::endl;
		std::cout << "Cloud resource in use " << m_mdmo.GetCloudResourceUsage() << std::endl;
		std::cout << "RAN resource in use " << m_mdmo.GetRanResourceUsage() << std::endl;
		std::cout << "Status " << m_mdmo.GetStatusStatistics() << std::endl;
//		m_mdmo.PrintResources();
	}
	void PrintSummaryShort() {
		std::cout << m_eval << "\t" << m_numJoinedAgvs << "\t0\t" << m_mdmo.GetCloudResourceUsage() << "\t0\t" << m_mdmo.GetRanResourceUsage() << "\t0\t"
				<< m_mdmo.GetStatusStatistics() << std::endl;
	}

private:

	uint32_t m_numJoinedAgvs;

	Mdmo m_mdmo;
	UeManager m_ueManager;
	EvalProfile m_eval;

};

#endif /* INCLUDE_SIMULATION_H_ */
