/*
 * mdmo.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_Mdmo_H_
#define INCLUDE_Mdmo_H_

#include <functional>

#include "agv-profile.h"
#include "interface/ue-mdmo.h"
#include "interface/mdmo-ran.h"
#include "interface/mdmo-cloud.h"
#include "resource-reservation-policy.h"
#include "ran-manager.h"
#include "cloud-manager.h"
#include "log.h"
#include "eval-profile.h"
#include "load-balancer.h"

enum MdmoStatus {
	HAVE_RESOURCES_MDMO_STATUS, RAN_RESOURCES_MISSING_MDMO_STATUS, CLOUD_RESOURCES_MISSING_MDMO_STATUS
};

class Mdmo {
public:

	Mdmo(EvalProfile eval) : m_ranManager(eval), m_cloudManager(eval, std::bind(&RanManager::GetRRHs, &m_ranManager)) {

		InstallApp(eval);
		m_mdmoStatus = HAVE_RESOURCES_MDMO_STATUS;
		m_c = 1;
	}
	~Mdmo() {

	}

	ResponseMdmo HandleRequest(RequestMdmo reqMdmo) {
		RM_FUNC_LOG(MANAGER_LOG);
		ResponseMdmo respMdmo(reqMdmo.agv);

		//
		// MDMO creates a request to RAN to provide options of resource allocation
		//
		auto inqRan = CreateRanInquiry(reqMdmo);
		//
		// RAN processes the request
		//
		auto respRan = m_ranManager.HandleInquiry(inqRan);
		//
		// if no resource allocation have been possible then exit
		//
		if (!respRan.success) {
			respMdmo.success = false;
			m_mdmoStatus = RAN_RESOURCES_MISSING_MDMO_STATUS;
			m_statusStat[RAN_RESOURCES_MISSING_MDMO_STATUS]++;
			return respMdmo;
		}

		ReserveOption ranReserveOption;
		do {
			//
			// MDMO selects one of the options provided by RAN
			//
			ranReserveOption = m_policy.SelectOption(respRan.options);
			//
			// MDMO creates a request to Cloud to provide an optimal for cloud option of resource allocation (only one option)
			//
			auto reqCloud = CreateCloudRequest(reqMdmo, ranReserveOption);
			//
			// Cloud processes the request
			//
			auto respCloud = m_cloudManager.HandleRequest(reqCloud);
			//
			// if cloud could find resources
			//
			if (respCloud.success) {
				break;
			}
			m_policy.RemoveOption(respRan.options, ranReserveOption);
		} while (!respRan.options.empty());

		//
		// if no resource allocation have been possible then exit
		//
		if (respRan.options.empty()) {
			respMdmo.success = false;
			m_mdmoStatus = CLOUD_RESOURCES_MISSING_MDMO_STATUS;
			m_statusStat[CLOUD_RESOURCES_MISSING_MDMO_STATUS]++;
			return respMdmo;
		} else {
			respMdmo.success = true;
		}

		//
		// Confirm reservation in RAN
		//
		m_ranManager.HandleRequest(CreateRanRequest(reqMdmo, ranReserveOption));
		m_mdmoStatus = HAVE_RESOURCES_MDMO_STATUS;

		if (m_eval.dynamicBalancing) UpdateApp();

		return respMdmo;
	}

	double GetCloudResourceUsage() {
		return m_cloudManager.GetResourceUsage();
	}
	double GetRanResourceUsage() {
		return m_ranManager.GetResourceUsage();
	}

	void Reset() {
		m_ranManager.Reset();
		m_cloudManager.Reset();
		m_statusStat[CLOUD_RESOURCES_MISSING_MDMO_STATUS] = 0;
		m_statusStat[RAN_RESOURCES_MISSING_MDMO_STATUS] = 0;
		m_c = 1;
		m_qosBalancer.Reset();
	}

	void PrintResources() {
		m_ranManager.PrintResources();
		m_cloudManager.PrintResources();
	}

	MdmoStatus GetStatus() {
		return m_mdmoStatus;
	}

	std::string GetStatusStatistics() {
		std::stringstream ss;
		ss << m_statusStat[CLOUD_RESOURCES_MISSING_MDMO_STATUS] << "\t" << m_statusStat[RAN_RESOURCES_MISSING_MDMO_STATUS];
		return ss.str();
	}

private:

	void InstallApp(EvalProfile eval) {
		m_eval = eval;
		AgvProfile agvProfile(eval);
		auto split = eval.qosSplit.MakeSplit(agvProfile.qos);
		auto ranProfile = agvProfile;
		ranProfile.qos = split.first;
		auto cloudProfile = agvProfile;
		cloudProfile.qos = split.second;

		m_ranManager.InstallApp(ranProfile);
		m_cloudManager.InstallApp(cloudProfile);
	}

	InquiryRan CreateRanInquiry(RequestMdmo reqMdmo) {
		RM_FUNC_LOG(MANAGER_LOG);
		InquiryRan req(reqMdmo.agv);
		req.t = reqMdmo.t;
		return req;
	}

	RequestCloud CreateCloudRequest(RequestMdmo reqMdmo, ReserveOption option) {
		RM_FUNC_LOG(MANAGER_LOG);
		RequestCloud req(reqMdmo.agv);
		req.rrhId = option.rrhId;
		return req;
	}

	RequestRan CreateRanRequest(RequestMdmo reqMdmo, ReserveOption option) {
		RM_FUNC_LOG(MANAGER_LOG);
		RequestRan req(reqMdmo.agv);
		req.option = option;
		req.t = reqMdmo.t;
		return req;
	}

	void UpdateApp() {
		auto uran = m_ranManager.GetResourceUsage();
		auto ucloud = m_cloudManager.GetResourceUsage();

		auto duran = uran - m_uran;
		auto ducloud = ucloud - m_ucloud;
		m_uran = duran;
		m_ucloud = ucloud;
		if (duran != 0 && ducloud != 0) {
			m_qosBalancer.Update(ducloud, duran);
			m_eval.qosSplit.sDatarate = m_qosBalancer.GetDatarateBalance();
			m_eval.qosSplit.sLatency = m_qosBalancer.GetLatencyBalance();
//			std::cout << (m_c++) << "\t\t" << m_eval.qosSplit.sDatarate << "\t\t" << m_eval.qosSplit.sLatency << std::endl;
			InstallApp(m_eval);
		}
	}

	RanManager m_ranManager;
	CloudManager m_cloudManager;
	ResoruceReservationPolicy m_policy;
	MdmoStatus m_mdmoStatus;
	std::map<MdmoStatus, uint32_t> m_statusStat;
	EvalProfile m_eval;
	QoSBalancer m_qosBalancer;
	uint32_t m_c;
	double m_uran;
	double m_ucloud;
};

#endif /* INCLUDE_Mdmo_H_ */
