/*
 * ran-reservation-policy.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_RESOURCE_RESERVATION_POLICY_H_
#define INCLUDE_RESOURCE_RESERVATION_POLICY_H_

#include <vector>
#include <assert.h>
#include "log.h"

struct ResoruceReservationPolicy {
	ReserveOption SelectOption(std::vector<ReserveOption> options) {
		assert(options.size() > 0);
		ReserveOption sel;
		sel.numRBs = 65000;
		sel.rrhId = 0;
		for (auto option : options) {
			if (option.numRBs < sel.numRBs) sel = option;
		}

		RM_LOG(MANAGER_LOG, "Selected an option to connect through RRH " << sel.rrhId);
		return sel;
	}

	void RemoveOption(std::vector<ReserveOption> &options, ReserveOption sel) {
		for (uint32_t i = 0; i < options.size(); i++) {
			if (sel.rrhId == options.at(i).rrhId) {
				options.erase(options.begin() + i, options.begin() + i + 1);
				break;
			}
		}
	}
};

#endif /* INCLUDE_RESOURCE_RESERVATION_POLICY_H_ */
