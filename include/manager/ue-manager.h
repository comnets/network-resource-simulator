/*
 * use-equipment.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_UE_MANAGER_H_
#define INCLUDE_UE_MANAGER_H_

#include <random>
#include <chrono>
#include <math.h>

#include "interface/ue-mdmo.h"
#include "cloud-app.h"
#include "eval-profile.h"
#include "resource-map.h"

class UeManager {

public:
	UeManager(EvalProfile eval) :
			time_distribution(0, NUM_TIME_SLOTS - 1) {
		m_cellSize = eval.cellSize;
		m_numAgvs = eval.numAgvs;
		m_numRrhs = eval.numRrhs;
		Create();
		m_numReqs = 0;
		generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
	}

	~UeManager() {

	}

	bool HaveAllTriedToJoin() {
		return (m_numReqs == m_numAgvs);
	}

	RequestMdmo CreateRequest() {
		RM_FUNC_LOG (MANAGER_LOG);
		assert(m_numReqs < m_numAgvs);
		assert(m_agvs.size() == m_numAgvs);

		RequestMdmo req(m_agvs.at(m_numReqs));
		req.t = time_distribution(generator);

		m_numReqs++;
		return req;
	}

	std::vector<AGV> GetAgvs() {
		return m_agvs;
	}

	void Reset() {
		m_numReqs = 0;
		m_agvs.clear();
		Create();
	}

private:

	void Create() {
		RM_FUNC_LOG (MANAGER_LOG);
		uint32_t c = 0;
		std::default_random_engine generator;
		std::uniform_real_distribution<> phi_distr(0, 2.0 * M_PI);
		std::uniform_real_distribution<> rad_distr(0, m_cellSize);
		RRHField field(m_numRrhs, m_cellSize);

		if (m_numRrhs == 1) {
			for (uint32_t i = 0; i < m_numAgvs; i++) {
				AGV agv;
				agv.id = c++;
				auto rrhId = 0;
				auto phi = phi_distr(generator);
				auto rad = rad_distr(generator);
				auto dx = rad * cos(phi);
				auto dy = rad * sin(phi);

				agv.x = field.rrhs.at(rrhId).x + dx;
				agv.y = field.rrhs.at(rrhId).y + dy;
				RM_LOG(MANAGER_LOG, "AGV " << agv.id << " at " << agv.x << ", " << agv.y);
				m_agvs.push_back(agv);
			}
		} else if (m_numRrhs == 2) {
			for (uint32_t i = 0; i < m_numAgvs; i++) {
				AGV agv;
				agv.id = c++;
				auto rrhId = i > (m_numAgvs >> 1) ? 0 : 1;
				auto phi = phi_distr(generator);
				auto rad = rad_distr(generator);
				auto dx = rad * cos(phi);
				auto dy = rad * sin(phi);

				agv.x = field.rrhs.at(rrhId).x + dx;
				agv.y = field.rrhs.at(rrhId).y + dy;
				RM_LOG(MANAGER_LOG, "AGV " << agv.id << " at " << agv.x << ", " << agv.y);
				m_agvs.push_back(agv);
			}
		} else {
			std::uniform_int_distribution<uint32_t> rrh_distr(0, m_numRrhs - 1);

			for (uint32_t i = 0; i < m_numAgvs; i++) {
				AGV agv;
				agv.id = c++;
				auto rrhId = rrh_distr(generator);
				auto phi = phi_distr(generator);
				auto rad = rad_distr(generator);
				auto dx = rad * cos(phi);
				auto dy = rad * sin(phi);

				agv.x = field.rrhs.at(rrhId).x + dx;
				agv.y = field.rrhs.at(rrhId).y + dy;
				RM_LOG(MANAGER_LOG, "AGV " << agv.id << " at " << agv.x << ", " << agv.y);
				m_agvs.push_back(agv);
			}
		}
	}

	//
	// time moment of joining request of a UE is randomly selected between 0 and 139 that corresponds to the transmission slots in RAN
	//
	std::default_random_engine generator;
	std::uniform_int_distribution<uint32_t> time_distribution;

	uint32_t m_numReqs;
	double m_cellSize;
	uint32_t m_numAgvs;
	uint32_t m_numRrhs;
	std::vector<AGV> m_agvs;
};

#endif /* INCLUDE_UE_MANAGER_H_ */
