/*
 * load-balancer.h
 *
 *  Created on: Apr 6, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_MANAGER_LOAD_BALANCER_H_
#define INCLUDE_MANAGER_LOAD_BALANCER_H_

#include <deque>
#include <stdint.h>
#include <numeric>
#include <cmath>
#include <iostream>

class LoadBalancer {
public:
	LoadBalancer() {
		m_min = 0;
		m_max = 1;
		m_v1.resize(m_depth);
		m_v2.resize(m_depth);
		for (uint16_t i = 0; i < m_depth; i++) {
			m_weight.push_back(1.0 / (double) (m_depth - i));
		}
		m_w = 0.5;
	}
	LoadBalancer(double min, double max, double w) {
		m_min = min;
		m_max = max;
		m_v1.resize(m_depth);
		m_v2.resize(m_depth);
		m_weight.resize(m_depth);
		for (uint16_t i = 0; i < m_depth; i++) {
//			m_weight.push_back(1.0 / (double) (m_depth - i));
			m_weight.push_back(pow(0.5, m_depth - i - 1));
		}
		m_w = w;
	}
	void SetLimits(double min, double max) {
		m_min = min;
		m_max = max;
	}
	void SetInitialBalance(double w) {
		m_w = w;
	}
	void Update(double v1, double v2) {
		m_v1.push_back(v1);
		m_v2.push_back(v2);
		if (m_v1.size() == m_depth + 1) m_v1.pop_front();
		if (m_v2.size() == m_depth + 1) m_v2.pop_front();

		auto wv1 = std::inner_product(std::begin(m_v1), std::end(m_v1), std::begin(m_weight), 0.0);
		auto wv2 = std::inner_product(std::begin(m_v2), std::end(m_v2), std::begin(m_weight), 0.0);

		m_w += (wv1 > wv2 ? -1 : 1) * m_step * std::fabs(m_max - m_min);
		m_w = (m_w > m_max) ? m_max : m_w;
		m_w = (m_w < m_min) ? m_min : m_w;
	}
	double Get() {
		return m_w;
	}
	void Reset()
	{
		m_v2.clear();
		m_v1.clear();
	}
private:
	/*
	 * balancing coefficient that allows a balancing between two systems with one output each
	 */
	double m_w;
	/*
	 * minimum and maximum values of the balancing coefficient
	 * approaching a minimum value should allow to relax the 1st system
	 * approaching a maximum value should allow to relax the 2nd system
	 */
	double m_min;
	double m_max;
	/*
	 * step size of m_w change, no unit, range (0,1]; value of 1 allows to change m_w from m_min to m_max at once (or from m_max to m_min)
	 */
	static constexpr double m_step = 0.05;
	/*
	 * previous output of the 1st system
	 */
	std::deque<double> m_v1;
	/*
	 * previous output of the 2nd system
	 */
	std::deque<double> m_v2;
	std::deque<double> m_weight;
	static const uint32_t m_depth = 10;
};

class QoSBalancer {
	/*
	 * consider the 1st system to be a cloud and the 2nd system to be a RAN
	 */
public:

	QoSBalancer() {
		Reset();
	}
	void Update(double v1, double v2) {
		m_l.Update(v1, v2);
		m_d.Update(v1, v2);
	}
	double GetLatencyBalance() {
		return m_l.Get();
	}
	double GetDatarateBalance() {
		return 1.0 / m_d.Get();
	}
	friend std::ostream& operator<<(std::ostream &os, QoSBalancer &obj) {
		os << "[ sD: " << obj.GetDatarateBalance() << ", sL: " << obj.GetLatencyBalance() << "]";
		return os;
	}
	void Reset() {
		m_l.SetLimits(0.2, 0.9);
		m_l.SetInitialBalance(0.5);
		m_d.SetLimits(1.0 / 8.0, 1.0 / 1.01);
		m_l.SetInitialBalance(1.0 / 2.0);
		m_l.Reset();
		m_d.Reset();
	}

private:

	LoadBalancer m_l;
	LoadBalancer m_d;
};

#endif /* INCLUDE_MANAGER_LOAD_BALANCER_H_ */
