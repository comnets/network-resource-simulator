/*
 * cloud-manager.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_CLOUD_MANAGER_H_
#define INCLUDE_CLOUD_MANAGER_H_

#include <functional>
#include <vector>
#include <chrono>

#include "agv-profile.h"
#include "app-profile.h"
#include "interface/mdmo-cloud.h"
#include "rrh.h"
#include "cloud.h"
#include "log.h"
#include "eval-profile.h"

class CloudManager {
public:
	CloudManager(EvalProfile eval, std::function<std::vector<RRH>()> cb) : m_cloud(eval) {
		m_edgeCapacity = eval.edgeCapacity;
		m_nodeCapacity = eval.nodeCapacity;
		m_numCloudNodes = eval.numCloudNodes;
		m_numConnectionPerCloudNode = eval.numConnectionPerCloudNode;
		m_eval = eval;
		m_getRrhs = cb;
		CreateCloud();
	}
	~CloudManager() {

	}

	ResponseCloud HandleRequest(RequestCloud req) {
		RM_FUNC_LOG(MANAGER_LOG);
		ResponseCloud resp(req.agv);
		auto success = m_cloud.DeployApp(m_agvProfile.app, m_agvProfile.qos, req.rrhId);
		resp.success = success;
		return resp;
	}

	void InstallApp(AgvProfile agvProfile) {
		RM_FUNC_LOG(MANAGER_LOG);
		RM_LOG(MANAGER_LOG, "Cloud QoS requirement: " << agvProfile.qos);
		this->m_agvProfile = agvProfile;
	}

	/*
	 * returns true if the node has at least one neighbor
	 */
	bool IsNodeConnected(uint32_t id) {
		return m_cloud.IsNodeConnected(id);
	}
	/*
	 * returns true if the RRH is assigned to a node
	 */
	bool IsRrhConnected(uint32_t id) {
		return m_cloud.IsRrhConnected(id);
	}
	NodeResource GetNodeUsage(uint32_t id) {
		return m_cloud.GetNodeUsage(id);
	}
	double GetResourceUsage() {
		return m_cloud.GetResourceUsage();
	}

	void Reset() {
		m_cloud.Reset();
		CreateCloud();
	}

	void PrintResources() {
		std::cout << std::endl << "Cloud resources" << std::endl << m_cloud << std::endl << std::endl;
	}

private:

	void CreateCloud() {
		RM_FUNC_LOG(MANAGER_LOG);
		//
		// create nodes
		//
		for (uint32_t i = 0; i < m_numCloudNodes; i++) {
			m_cloud.AddNode(i, m_nodeCapacity);
		}
		//
		// connect nodes to remote radio heads
		// practically some computing nodes will reside on RRHs
		//
		auto rrhs = m_getRrhs();
		std::default_random_engine generator;
		generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
		std::uniform_int_distribution<uint32_t> distribution(0, m_numCloudNodes - 1);

		assert(m_numCloudNodes >= rrhs.size());
		for (auto rrh : rrhs) {
			uint32_t nodeId = 0;
			do {
				nodeId = distribution(generator);
			} while (m_cloud.HasRrhConnected(nodeId));
			m_cloud.ConnectRrh(nodeId, rrh.id);
		}
		//
		// connect cloud nodes between each other
		//
		for (uint32_t i = 0; i < m_numCloudNodes; i++) {
			for (uint32_t j = 0; j < m_numConnectionPerCloudNode; j++) {
				uint32_t nodeId = 0;
				do {
					nodeId = distribution(generator);
				} while (nodeId == i || m_cloud.HasNodeConnected(i, nodeId));
				m_cloud.Connect(i, nodeId, m_edgeCapacity);
			}
		}

		m_cloud.CalcMaxCloudResources();
	}

	Cloud m_cloud;
	uint32_t m_numCloudNodes;
	uint32_t m_numConnectionPerCloudNode;
	std::function<std::vector<RRH>()> m_getRrhs;
	AgvProfile m_agvProfile;
	NodeResource m_nodeCapacity;
	EdgeResource m_edgeCapacity;
	EvalProfile m_eval;
};

#endif /* INCLUDE_CLOUD_MANAGER_H_ */
