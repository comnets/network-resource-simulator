/*
 * ran-manager.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_RAN_MANAGER_H_
#define INCLUDE_RAN_MANAGER_H_

#include <vector>
#include <math.h>
#include <limits>

#include "interface/mdmo-ran.h"
#include "agv-profile.h"
#include "rrh.h"
#include "channel.h"
#include "effective-snr.h"
#include "phy.h"
#include "dll.h"
#include "log.h"
#include "eval-profile.h"

class RanManager {
public:
	RanManager(EvalProfile eval) {
		m_numRrhs = eval.numRrhs;
		m_cellSize = eval.cellSize;
		CreateRan();
	}
	~RanManager() {

	}

	ResponseRan HandleInquiry(InquiryRan req) {
		RM_FUNC_LOG(MANAGER_LOG);
		ResponseRan resp(req.agv);
		auto agv = req.agv;
		RM_LOG(MANAGER_LOG, "Received inquiry for UE " << req.agv.id);

		for (uint32_t i = 0; i < m_rrhs.size(); i++) {

			auto rrh = m_rrhs.at(i);
			RM_LOG(MANAGER_LOG, "Check an option to connect through RRH " << rrh.id);
			uint32_t rbs_prev = 0, c = 0, rbs = 1;
			std::vector<ResourceBlockGroup> resourceGroups;
			double phyEfficiency = m_phy.at(rrh.id).GetMaxEfficiency();

			//
			// get SNRs of main signal and interference firstly independent on the actual time and spectrum usage
			//
			RanPtpChannel ptpChannel;
			RM_LOG(MANAGER_LOG, "Calculate SNR");
			Snr snrMain = ptpChannel.GetSnr(Position(agv.x, agv.y), Position(rrh.x, rrh.y));
			std::map<uint32_t, Power> powerInterference;
			for (auto rrhI : m_rrhs) {
				if (rrhI.id == rrh.id) continue;
				assert(rrhI.id < m_dll.size());
				RM_LOG(MANAGER_LOG, "Calculate INR for RRH " << rrhI.id);
				powerInterference[rrhI.id] = ptpChannel.GetSnr(Position(rrhI.x, rrhI.y), Position(rrh.x, rrh.y)).GetPower();
				RM_LOG(MANAGER_LOG, "Interference power from " << rrhI.id << ": " << powerInterference[rrhI.id]);
			}
			//
			// iteratively update PHY efficiency /phyEfficiency/ and and the access plan /resourceGroups/
			// until the access plan does not change any more
			//
			while (rbs > rbs_prev) {

				RM_LOG(MANAGER_LOG, "Start iteration " << c + 1);
				rbs_prev = rbs;

				if (c != 0) {
					//
					// calculate PHY efficiency /phyEfficiency/ as an average PHY efficiency over all time slots
					// corresponding to the actual access plan /resourceGroups/
					//
					uint32_t ts = NUM_TIME_SLOTS;
					std::vector<double> phyEfficiencies(ts, 0);
					std::vector<uint32_t> rbsAtT(ts, 0);

					for (uint32_t t = 0; t < ts; t++) {
//						RM_LOG(MANAGER_LOG, "Working for time slot " << t);
						Snr sinr;
						Power noise;
						std::vector<std::vector<uint32_t> > interferingRrhs(Carriers::maxRe, std::vector<uint32_t>(0));
						for (uint32_t f = 0; f < Carriers::maxRe; f++) {
//							RM_LOG(MANAGER_LOG, "Working for RE group " << f);
//							RM_LOG(MANAGER_LOG, "Number of resource groups " << resourceGroups.size());
							for (auto resourceGroup : resourceGroups) {
//								RM_LOG(MANAGER_LOG, "Number of RBs " << resourceGroup.size());
								for (auto rb : resourceGroup) {
//									RM_LOG(MANAGER_LOG, "RB " << rb);
									if (rb.startRe == f && rb.startT == t) {
//										RM_LOG(MANAGER_LOG, "Got it!");
										rbsAtT.at(t) = rbsAtT.at(t) + 1;
										sinr.Set(snrMain.Get(f), f);
										for (auto rrhI : m_rrhs) {
											if (rrhI.id == rrh.id) continue;
											assert(rrhI.id < m_dll.size());
											if (m_dll.at(rrhI.id).IsReservedAt(t, f)) {
												noise.Add(powerInterference[rrhI.id].Get(f), f);
											}
										}
									}
								}
							}
						}

						auto snr = sinr;
						sinr.AddNoise(noise);
						auto effSinr = EffectiveSnr::GetEffSnr(sinr);
						assert(rrh.id < m_phy.size());
						phyEfficiencies.at(t) = m_phy.at(rrh.id).CalcEfficiency(effSinr);
						RM_LOG(MANAGER_LOG, "PHY efficiency " << phyEfficiencies.at(t) << " at time slot " << t << " for " << rbsAtT.at(t) << " RBs");
//						if (phyEfficiencies.at(t) == 0 && rbsAtT.at(t) != 0) {
//							RM_LOG(MANAGER_LOG, "SNR " << snr << ", SINR " << sinr << ", Interference noise power " << noise);
//							RM_LOG(MANAGER_LOG, "Effective SINR " << effSinr);
//						}
					}
					//
					// get average PHY efficiency per resource block group
					//
					phyEfficiency = 0;
					uint32_t numRbs = 0;
					for (uint32_t t = 0; t < ts; t++) {
						phyEfficiency += (phyEfficiencies.at(t) * (double) rbsAtT.at(t));
						numRbs += rbsAtT.at(t);
					}
					if (numRbs == 0) {
						RM_LOG(MANAGER_LOG, "Number of resource groups " << resourceGroups.size());
						for (auto resourceGroup : resourceGroups) {
							RM_LOG(MANAGER_LOG, "Number of RBs " << resourceGroup.size());
							for (auto rb : resourceGroup) {
								RM_LOG(MANAGER_LOG, "RB " << rb);
							}
						}
					}
					phyEfficiency = (numRbs == 0) ? 0 : phyEfficiency / (double) numRbs;
					RM_LOG(MANAGER_LOG, "Average PHY efficiency " << phyEfficiency << " over " << numRbs << " RBs");
				}
				if (phyEfficiency < m_phy.at(rrh.id).GetMinEfficiency()) {
					RM_LOG(MANAGER_LOG, "No PHY resources");
					break;
				}
				//
				// make actual plan
				//
				resourceGroups = m_dll.at(rrh.id).PlanResources(phyEfficiency, m_agvProfile.qos, req.t);
				if (!m_dll.at(rrh.id).HaveResources()) {
					RM_LOG(MANAGER_LOG, "No DLL resources");
					break;
				}
				rbs = m_dll.at(rrh.id).GetNumRbs();
				RM_LOG(MANAGER_LOG, "RBs " << rbs << ", RBs (old) " << rbs_prev << " s " << resourceGroups.size());
				assert(c++ < 100);
			}

			if (phyEfficiency >= m_phy.at(rrh.id).GetMinEfficiency() && m_dll.at(rrh.id).HaveResources()) {
				ReserveOption option;
				option.numRBs = m_dll.at(rrh.id).GetNumRbs();
				option.resourceGroups = resourceGroups;
				option.rrhId = rrh.id;
				resp.options.push_back(option);
				resp.success = true;
				RM_LOG(MANAGER_LOG, "Adding RAN option through RRH " << option.rrhId << " number RBs " << option.numRBs);
			}
		}
		if (resp.options.empty()) resp.success = false;
		return resp;
	}

	void HandleRequest(RequestRan req) {
		RM_FUNC_LOG(MANAGER_LOG);
		assert(req.option.rrhId < m_rrhs.size());
		//
		// DLL keeps the last access plan since the last inquiry
		// the resources for this plan will be reserved with the command below
		//
		m_dll.at(req.option.rrhId).ReserveResources(req.option.resourceGroups);
	}

	double GetResourceUsage() {
		double r = 0;
		for (auto rrh : m_rrhs) {
			r += m_dll.at(rrh.id).GetResourceUsage();
		}
		return r / (double) m_dll.size();
	}

	std::vector<RRH> GetRRHs() {
		return m_rrhs;
	}

	void InstallApp(AgvProfile agvProfile) {
		RM_FUNC_LOG(MANAGER_LOG);
		RM_LOG(MANAGER_LOG, "RAN QoS requirement: " << agvProfile.qos);
		this->m_agvProfile = agvProfile;
	}
	void Reset() {
		m_dll.clear();
		m_phy.clear();
		m_rrhs.clear();
		CreateRan();
	}

	void PrintResources() {
		for (uint32_t i = 0; i < m_rrhs.size(); i++) {
			std::cout << std::endl << "Resource map for RRH " << i << std::endl << m_dll.at(i) << std::endl << std::endl;
		}
	}

private:

	void CreateRan() {
		RM_FUNC_LOG(MANAGER_LOG);

		RRHField field(m_numRrhs, m_cellSize);
		m_rrhs = field.rrhs;
		for (auto rrh : m_rrhs) {
			m_phy.push_back(RanPhy());
			m_dll.push_back(RanDll());

		}
	}

	double m_numRrhs;
	double m_cellSize;
	std::vector<RRH> m_rrhs;
//	RanChannel m_channel;
	std::vector<RanPhy> m_phy;
	std::vector<RanDll> m_dll;
	AgvProfile m_agvProfile;
};

#endif /* INCLUDE_RAN_MANAGER_H_ */
