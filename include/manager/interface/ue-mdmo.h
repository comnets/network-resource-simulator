/*
 * ue-mdmo.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_INTERFACE_UE_MDMO_H_
#define INCLUDE_INTERFACE_UE_MDMO_H_

#include "interface.h"

struct RequestMdmo: public Request {
	RequestMdmo(AGV other) :
			Request(other) {
		t = 0;
	}

	// moment of time within the frame duration given in number of OFDM symbols
	// hence the time unit here is the number of OFDM symbols since the start of the frame
	uint32_t t;
};

struct ResponseMdmo: public Response {
	ResponseMdmo(AGV other) :
			Response(other) {
		success = false;
	}

	bool success;
};

#endif /* INCLUDE_INTERFACE_UE_MDMO_H_ */
