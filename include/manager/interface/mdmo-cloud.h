/*
 * mdmo-cloud.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_INTERFACE_MDMO_CLOUD_H_
#define INCLUDE_INTERFACE_MDMO_CLOUD_H_

#include "interface.h"

struct RequestCloud: public Request {
	RequestCloud(AGV other) :
			Request(other) {
		rrhId = -1;
	}
	uint32_t rrhId;
};

struct ResponseCloud: public Response {
	ResponseCloud(AGV other) :
			Response(other) {
		success = false;
	}

	bool success;
};

#endif /* INCLUDE_INTERFACE_MDMO_CLOUD_H_ */
