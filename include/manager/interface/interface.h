/*
 * interface.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_INTERFACE_INTERFACE_H_
#define INCLUDE_INTERFACE_INTERFACE_H_

#include <stdint.h>

#include "agv.h"

struct Request {
	Request(AGV other) :
			agv(other) {
	}
	AGV agv;
};

struct Response {
	Response(AGV other) :
			agv(other) {
	}
	AGV agv;
};


#endif /* INCLUDE_INTERFACE_INTERFACE_H_ */
