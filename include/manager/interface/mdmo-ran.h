/*
 * mdmo-ran.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_INTERFACE_MDMO_RAN_H_
#define INCLUDE_INTERFACE_MDMO_RAN_H_

#include "interface.h"
#include "resources.h"

struct ReserveOption {

	ReserveOption() {
		numRBs = -1;
		rrhId = -1;
	}

	ReserveOption& operator=(const ReserveOption &other) {
		// check for self-assignment
		if (&other == this)
			return *this;
		// reuse storage when possible
		numRBs = other.numRBs;
		rrhId = other.rrhId;
		resourceGroups = other.resourceGroups;
		return *this;
	}

	uint32_t numRBs;
	uint32_t rrhId;
	std::vector<ResourceBlockGroup> resourceGroups;
};

struct InquiryRan: public Request {
	InquiryRan(AGV other) :
			Request(other) {
		t = 0;
	}

	// moment of time within the frame duration given in number of OFDM symbols
	// hence the time unit here is the number of OFDM symbols since the start of the frame
	uint32_t t;
};

struct ResponseRan: public Response {
	ResponseRan(AGV other) :
			Response(other) {
		success = false;
	}

	std::vector<ReserveOption> options;

	bool success;
};

struct RequestRan: public Request {
	RequestRan(AGV other) :
			Request(other) {
		t = 0;
	}

	ReserveOption option;
	// moment of time within the frame duration given in number of OFDM symbols
	// hence the time unit here is the number of OFDM symbols since the start of the frame
	uint32_t t;
};

#endif /* INCLUDE_INTERFACE_MDMO_RAN_H_ */
