/*
 * log.h
 *
 *  Created on: 02.10.2016
 *      Author: tsokalo
 */

#ifndef LOG_H_
#define LOG_H_

#include <iostream>
#include <string.h>

//#define CLOUD_LOG	            1
//#define TEST_LOG	            1
//#define CHANNEL_LOG	            1
//#define MANAGER_LOG	            1
//#define DLL_LOG		            1
//#define APP_LOG		            1
//#define STATS_LOG	            1

#define CLOUD_LOG	            0
#define TEST_LOG	            0
#define CHANNEL_LOG	            0
#define MANAGER_LOG	            0
#define DLL_LOG		            0
#define APP_LOG		            0
#define STATS_LOG	            0

inline std::string methodName(const std::string& prettyFunction)
{
    size_t colons = prettyFunction.find("::");
    size_t begin = prettyFunction.substr(0,colons).rfind(" ") + 1;
    size_t end = prettyFunction.rfind("(") - begin;

    return prettyFunction.substr(begin,end) + "()";
}

#define __METHOD_NAME__ methodName(__PRETTY_FUNCTION__)

#define RM_LOG(condition, message) \
    do { \
    if (condition) { \
    std::cout << "`" #condition "`:\t" << \
	__METHOD_NAME__ << " :\t" << \
    "`" << message << "`" << std::endl; \
    } \
    } while (false)


#define RM_FUNC_LOG(condition) \
    do { \
	if (condition) { \
    std::cout << "`" #condition "`:\t" << \
    __METHOD_NAME__ << std::endl; \
	} \
    } while (false)

#endif /* LOG_H_ */
