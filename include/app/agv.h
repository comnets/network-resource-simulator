/*
 * agv.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_AGV_H_
#define INCLUDE_AGV_H_

#include <stdint.h>

struct AGV {
	AGV() {
		id = 0;
		x = 0;
		y = 0;

	}
	AGV(const AGV &agv) {
		id = agv.id;
		x = agv.x;
		y = agv.y;
	}
	AGV& operator=(const AGV &agv) {
		// check for self-assignment
		if (&agv == this)
			return *this;
		id = agv.id;
		x = agv.x;
		y = agv.y;
		return *this;
	}

	uint32_t id;
	//position
	double x;
	double y;
};

#endif /* INCLUDE_AGV_H_ */
