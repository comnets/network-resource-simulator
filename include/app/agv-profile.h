/*
 * agv-app.h
 *
 *  Created on: Feb 24, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_APP_AGV_PROFILE_H_
#define INCLUDE_APP_AGV_PROFILE_H_

#include "app-profile.h"
#include "eval-profile.h"

struct AgvProfile: public AppProfile {

	AgvProfile(){
		qos = QoS(63, 0.0064, 10, 10);
		nodeRequirement = NodeRequirement(100,100,100);
		edgeRequirement = EdgeRequirement(100);
		numMicroServices = 1;
	}

	AgvProfile(EvalProfile eval) {
		qos = eval.appQos;
		numMicroServices = eval.numMicroServices;
		nodeRequirement = eval.nodeRequirement;
		edgeRequirement = eval.edgeRequirement;
		CreateApp();
	}

//	AgvProfile(const AppProfile &appProfile) :
//			AppProfile(appProfile.qos, appProfile.app) {
//
//		numMicroServices = 2;
//	}
//
//	AgvProfile(QoS qos, CloudApp app) :
//			AppProfile(qos, app) {
//
//		numMicroServices = 2;
//	}

	void CreateApp() {

		app.appId = 1;
		app.AddFirstMicroService(0, 1, edgeRequirement);
		for (uint32_t i = 1; i < numMicroServices; i++) {
			app.AddMicroService(i, i + 1, nodeRequirement, edgeRequirement);
		}
		app.AddLastMicroService(numMicroServices, nodeRequirement, edgeRequirement);
	}

	uint32_t numMicroServices; // not counting the fake micro-services
	NodeRequirement nodeRequirement;
	EdgeRequirement edgeRequirement;
};

#endif /* INCLUDE_APP_AGV_PROFILE_H_ */
