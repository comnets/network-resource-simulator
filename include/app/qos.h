/*
 * qos.h
 *
 *  Created on: Feb 18, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_QOS_H_
#define INCLUDE_QOS_H_

#include <stdint.h>
#include <vector>
#include <limits>

struct RB {
	// starting OFDM symbol within the frame
	uint32_t sym;
	// starting carrier within the OFDM symbol (same for all symbols in one RB)
	uint32_t carrier;
};

typedef std::vector<RB> allocation_t;

struct QoS {

	QoS() {
		messageSize = 0;
		datarate = std::numeric_limits<double>::max();
		latency = 0;
		jitter = 0;
	}
	QoS(double messageSize, double datarate, double latency, double jitter) {
		this->messageSize = messageSize;
		this->datarate = datarate;
		this->latency = latency;
		this->jitter = jitter;
	}

	QoS& operator=(const QoS &qos) {
		// check for self-assignment
		if (&qos == this) return *this;
		messageSize = qos.messageSize;
		datarate = qos.datarate;
		latency = qos.latency;
		jitter = qos.jitter;
		return *this;
	}

	friend QoS operator+(QoS lhs, const QoS &rhs) {
		lhs.datarate = 1.0 / (1.0 / lhs.datarate + 1.0 / rhs.datarate);
		lhs.latency += rhs.latency;
		lhs.jitter += rhs.jitter;
		return lhs;
	}

	friend QoS operator*(QoS lhs, const uint32_t n) {

		QoS qos;
		for (uint32_t i = 0; i < n; i++) {
			qos = qos + lhs;
		}
		return qos;
	}

	friend QoS operator-(QoS lhs, const QoS &rhs) {
		lhs.datarate = 1.0 / (1.0 / lhs.datarate - 1.0 / rhs.datarate);
		lhs.latency -= rhs.latency;
		lhs.jitter -= rhs.jitter;
		return lhs;
	}

	/*
	 * return true if 'this' violates 'targetQos'
	 */
	inline bool Violate(QoS &targetQos) {
		return targetQos.datarate > this->datarate || targetQos.latency < this->latency || targetQos.jitter < this->jitter;
	}

	friend std::ostream& operator<<(std::ostream &os, const QoS &obj) {
		os << "[ Message size: " << obj.messageSize << ", D: " << obj.datarate << ", L: " << obj.latency << ", J: " << obj.jitter << "]";
		return os;
	}

	double messageSize; // unit [bytes]
	double datarate; // unit [Mbps]
	double latency; // unit [milliseconds]
	double jitter; // unit [milliseconds]
};

struct QosSplit {

	QosSplit() {
		this->sDatarate = 2;
		this->sLatency = 0.5;
		this->sJitter = 0.5;
	}

	QosSplit(double sDatarate, double sLatency, double sJitter) {
		this->sDatarate = sDatarate;
		this->sLatency = sLatency;
		this->sJitter = sJitter;
		assert(sDatarate > 1.0);
		assert(sLatency < 1.0);
		assert(sJitter < 1.0);
	}

	std::pair<QoS, QoS> MakeSplit(QoS qos) {
		std::pair<QoS, QoS> split;
		split.first = qos;
		split.second = qos;
		split.first.datarate = qos.datarate * sDatarate;
		split.first.latency = qos.latency * sLatency;
		split.first.jitter = qos.jitter * sJitter;
		split.second = qos - split.first;
		return split;
	}

	friend std::ostream& operator<<(std::ostream &os, const QosSplit &obj) {
		os << "[ sD: " << obj.sDatarate << ", sL: " << obj.sLatency << ", sJ: " << obj.sJitter << "]";
		return os;
	}

	double sDatarate;
	double sLatency;
	double sJitter;
};

typedef QoS Performance;

#endif /* INCLUDE_QOS_H_ */
