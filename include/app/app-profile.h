/*
 * app-profile.h
 *
 *  Created on: Feb 23, 2020
 *      Author: tsokalo
 */

#ifndef INCLUDE_APP_PROFILE_H_
#define INCLUDE_APP_PROFILE_H_

#include "cloud-app.h"
#include "qos.h"

struct AppProfile {

	AppProfile()
	{

	}
	AppProfile(QoS qos, CloudApp app)
		{
			this->qos = qos;
			this->app = app;
		}
	AppProfile& operator=(const AppProfile &obj) {
		// check for self-assignment
		if (&obj == this)
			return *this;
		qos = obj.qos;
		app = obj.app;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream& os, const AppProfile& obj)
		{
			os << "QoS " << obj.qos << ", App " << obj.app;
			return os;
		}
	QoS qos;
	CloudApp app;
};

#endif /* INCLUDE_APP_PROFILE_H_ */
