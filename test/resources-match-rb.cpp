/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "resource-map.h"

TEST(resourcemap, resources) {
	ResourceMap rMap;

	auto n = rMap.GetNumTimeSlots();
	for (uint32_t i = 0; i < n; i++) {
		GTEST_ASSERT_LE(rMap.GetVacantAt(i), 100); //Carriers::maxRe
	}
}

TEST(resourcemap, matchrb) {
	ResourceMap rMap;

	ResourceBlock rb;
	rb.startRe = 0;
	rb.startT = 0;

	for (uint32_t j = 0; j < 21; j++) {
		for (uint32_t i = 0; i < 100; i++) {
			if (rMap.Match(rb, 0)) {
//				RM_LOG(TEST_LOG, "RB " << rb << "\n" << rMap);
				GTEST_ASSERT_EQ(rb.startRe, i);
				GTEST_ASSERT_EQ(rb.startT, j);
				ResourceBlockGroup rbg = { rb };
				rMap.Reserve(rbg);
			} else {
				break;
			}
		}
	}
}

TEST(resourcemap, matchgroup) {

	ResourceMap rMap;

	ResourceBlockGroup rbg;
	rbg.resize(10);

	if (rMap.Match(rbg, 0, 10)) {
		GTEST_ASSERT_EQ(rbg.begin()->startRe, 0);
		GTEST_ASSERT_EQ(rbg.begin()->startT, 0);
		rMap.Reserve(rbg);
	} else {

	}
}

TEST(resourcemap, countcarriers) {
	ResourceMap rmap;
	ResourceBlock rb;
	rb.startRe = 0;
	rb.startT = 0;
	ResourceBlockGroup rbg = { rb };

	GTEST_ASSERT_EQ(rmap.GetRes(rbg), 84);
}

TEST(resourcemap, rbsequence) {
	ResourceMap rmap;
	ResourceBlockGroup rbg;
	rmap.Match(rbg, 0, rmap.GetVacantAt(0) * ResourceBlock::numT);

	GTEST_ASSERT_EQ(rbg.at(0).startT, 0);
	GTEST_ASSERT_EQ(rbg.at(0).startRe, 0);
	GTEST_ASSERT_EQ(rbg.at(1).startT, 0);
	GTEST_ASSERT_EQ(rbg.at(1).startRe, 1);
	GTEST_ASSERT_EQ(rbg.at(2).startT, 0);
	GTEST_ASSERT_EQ(rbg.at(2).startRe, 2);
}

