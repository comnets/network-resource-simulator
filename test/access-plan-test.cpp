/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "access-plan.h"
#include "log.h"

TEST(accessplan, makegroup) {
	ResourceBlockGroup rbg;
	ResourceMap rmap;
	uint32_t tau = 0;
	uint32_t numRbs = 2;
	uint32_t numBits = 10000;
	double bpcs = 1.0;

	bool success = rmap.Match(rbg, tau, ceil((double)numBits / bpcs));

//	rbg.resize(numRbs - 1);
//	do {
//		rbg.resize(rbg.size() + 1);
//		//
//		// find resources for one group
//		//
//		bool success = rmap.Match(rbg, tau);
//		if (success) {
//			//
//			// one resource block group was successfully scheduled
//			//
//		} else {
//			//
//			// no resources available
//			//
//			break;
//		}
//	} while (rmap.GetRes(rbg) * bpcs < numBits);

	GTEST_ASSERT_EQ(rbg.size(), 167);
}

TEST(accessplan, latencyvalidity) {
	ResourceMap rmap;
	QoS qos;
	qos.messageSize = 100;
	qos.datarate = 10;
	qos.latency = 5;
	qos.jitter = 5;

	auto maxSlot = ceil(qos.latency / 1000.0 / OFDM_SYM_DURATION) - ResourceBlock::numT - ceil(FRONT_END_DELAY / OFDM_SYM_DURATION);
	RM_LOG(TEST_LOG, "Maximum buffer time [number of RBs]: " << ceil((double) maxSlot / (double) ResourceBlock::numT));
	AccessPlan plan(1.0, qos);
	ResourceBlock rb;
	rb.startRe = 0;
	{
		rb.startT = 0;
		ResourceBlockGroup rbg = { rb };
		GTEST_ASSERT_EQ(plan.IsLatencyValid(rbg, 0, rmap), true);
	}
	{
		rb.startT = floor((double) maxSlot / (double) ResourceBlock::numT);
		ResourceBlockGroup rbg = { rb };
		GTEST_ASSERT_EQ(plan.IsLatencyValid(rbg, 0, rmap), true);
	}
	{
		rb.startT = ceil((double) maxSlot / (double) ResourceBlock::numT);
		ResourceBlockGroup rbg = { rb };
		GTEST_ASSERT_EQ(plan.IsLatencyValid(rbg, 0, rmap), false);
	}

}

TEST(accessplan, full) {
	ResourceMap rmap;
	QoS qos;
	qos.messageSize = 100;
	qos.datarate = 1.0;
	qos.latency = 5;
	qos.jitter = 5;
	double bpcs = 1.0;
	AccessPlan plan(bpcs, qos);
	plan.Make(0, rmap);
	GTEST_ASSERT_EQ(plan.IsValid(), true);

	//
	// ensure correct number of transmission opportunities planned for this traffic
	//
	auto iat = qos.messageSize * 8 / qos.datarate / 1024 / 1024;
	auto ts = rmap.GetNumTimeSlots() * OFDM_SYM_DURATION * (double) ResourceBlock::numT;
	auto numGroups = floor(ts / iat);
	auto groups = plan.GetGroups();
	GTEST_ASSERT_EQ(groups.size(), numGroups);

	//
	// ensure an adequate number of resource block per message
	//
	auto numRBs = ceil(qos.messageSize * 8 / (double) ResourceBlock::numRe / (double) ResourceBlock::numT / bpcs);
	for (auto rbg : groups) {
		GTEST_ASSERT_GE(rbg.size(), numRBs);
	}
}

