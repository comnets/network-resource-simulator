/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "resource-map.h"
#include "qos.h"
#include "dll.h"
#include "log.h"

TEST(dll, full) {

	RanDll dll;

	QoS qos;
	qos.datarate = 0.2;
	qos.latency = 2000;
	qos.jitter = 2000;
	double bpcs = 1.0;
	uint32_t t = 0;

	ResourceMap rmap;
	auto ts = rmap.GetNumTimeSlots();
	auto fs = Carriers::maxRe;
	double tfs = 0;
	for (uint32_t t = 0; t < ts; t++) {
		tfs += rmap.GetVacantAt(t);
	}
	tfs *= ResourceBlock::numRe;
	tfs *= bpcs;
	tfs *= ResourceBlock::numT;
	RM_LOG(TEST_LOG, "Max. number of bits " << tfs);
	qos.messageSize = ceil(tfs / 8.0);
	RM_LOG(TEST_LOG, "Set message size to " << qos.messageSize);

	ASSERT_DOUBLE_EQ(dll.GetResourceUsage(), 0.0);
	auto resourceGroups = dll.PlanResources(bpcs, qos, t);
	dll.ReserveResources(resourceGroups);
	RM_LOG(TEST_LOG, "Resource usage: " << dll.GetResourceUsage());
	ASSERT_NEAR(dll.GetResourceUsage(), 1.0, 0.001);
}

TEST(dll, admissionprobability) {

	RanDll dll;

	QoS qos;
	qos.datarate = 0.2;
	qos.latency = 4;
	qos.jitter = 2000;
	double bpcs = 1.0;
	uint32_t t = 0;

	auto maxSlot = floor(qos.latency / 1000.0 / OFDM_SYM_DURATION) - ResourceBlock::numT - floor(FRONT_END_DELAY / OFDM_SYM_DURATION);

	RM_LOG(TEST_LOG, "Number of transmission slots " << maxSlot);
	ResourceMap rmap;
	auto ts = maxSlot;
	auto fs = Carriers::maxRe;
	double tfs = 0;
	for (uint32_t t = 0; t < ts; t++) {
		tfs += rmap.GetVacantAt(t);
	}
	tfs *= ResourceBlock::numRe;
	tfs *= bpcs;
	RM_LOG(TEST_LOG, "Max. number of bits " << tfs);
	qos.messageSize = floor(tfs / 8.0);
	RM_LOG(TEST_LOG, "Set message size to " << qos.messageSize);

	auto resourceGroups = dll.PlanResources(bpcs, qos, t);
	ASSERT_TRUE(dll.HaveResources());
	dll.ReserveResources(resourceGroups);
	resourceGroups = dll.PlanResources(bpcs, qos, t);
	ASSERT_TRUE(!dll.HaveResources());
	qos.latency = 8;
	resourceGroups = dll.PlanResources(bpcs, qos, t);
	ASSERT_TRUE(dll.HaveResources());
	dll.ReserveResources(resourceGroups);
	resourceGroups = dll.PlanResources(bpcs, qos, t);
	ASSERT_TRUE(!dll.HaveResources());
}

