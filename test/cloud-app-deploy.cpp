/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "cloud-manager.h"
#include "ran-manager.h"
#include "ue-manager.h"
#include "log.h"
#include "eval-profile.h"

TEST(cloud, deployallonone) {

	EvalProfile eval;
	eval.numRrhs = 10;
	eval.cellSize = 10;
	eval.numAgvs = 100;
	RanManager ranManager(eval);
	auto rrhs = ranManager.GetRRHs();

	//
	// create cloud manager
	//
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);
	eval.numCloudNodes = 100;
	eval.numConnectionPerCloudNode = 5;
	CloudManager cloudManager(eval, std::bind(&RanManager::GetRRHs, &ranManager));

	//
	// create an AGV profile
	//
	CloudApp app;
	eval.numMicroServices = 5; // not counting the fake micro-services
	eval.nodeRequirement = NodeRequirement(20, 20, 20);
	eval.edgeRequirement = EdgeRequirement(20);

	app.appId = 1;
	app.AddFirstMicroService(0, 1, eval.edgeRequirement);
	for (uint32_t i = 1; i < eval.numMicroServices; i++) {
		app.AddMicroService(i, i + 1, eval.nodeRequirement, eval.edgeRequirement);
	}
	app.AddLastMicroService(eval.numMicroServices, eval.nodeRequirement, eval.edgeRequirement);

	eval.appQos = QoS(1000, 20, 5, 5);

	AgvProfile agvProfile(eval);

	//
	// install app on the cloud manager
	//
	RM_LOG(TEST_LOG, "Created profile " << agvProfile);
	cloudManager.InstallApp(agvProfile);

	//
	// create UE manager
	//
	UeManager ueManager(eval);

	//
	// handle UE join request in cloud
	//
	auto agvs = ueManager.GetAgvs();
	auto oagv = agvs.at(0);
	auto orrh = rrhs.at(0);

	RequestCloud reqCloud(oagv);
	reqCloud.rrhId = orrh.id;
	auto resp = cloudManager.HandleRequest(reqCloud);
	GTEST_ASSERT_EQ(resp.success, true);

	//
	// there should be only one cloud node used
	//
	uint32_t numberFullNodes = 0;
	uint32_t numbeEmptyNodes = 0;
	NodeResource noUsage(0, 0, 0);
	for (uint32_t i = 0; i < eval.numCloudNodes; i++) {
		auto usage = cloudManager.GetNodeUsage(i);
		RM_LOG(TEST_LOG, "Node " << i << " usage " << usage.cpu << "\t" << usage.ram << "\t" << usage.flash);
		if (usage >= eval.nodeCapacity) {
			numberFullNodes++;
		}
		if (noUsage >= usage) {
			numbeEmptyNodes++;
		}
	}
	GTEST_ASSERT_EQ(numberFullNodes == 1, true);
	GTEST_ASSERT_EQ(numbeEmptyNodes == eval.numCloudNodes - numberFullNodes, true);
}

TEST(cloud, deployonemore) {

	EvalProfile eval;
	eval.numRrhs = 10;
	eval.cellSize = 10;
	eval.numAgvs = 100;
	RanManager ranManager(eval);
	auto rrhs = ranManager.GetRRHs();

	//
	// create cloud manager
	//
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);
	eval.numCloudNodes = 100;
	eval.numConnectionPerCloudNode = 5;
	CloudManager cloudManager(eval, std::bind(&RanManager::GetRRHs, &ranManager));

	//
	// create an AGV profile
	//
	CloudApp app;
	eval.numMicroServices = 5; // not counting the fake micro-services
	eval.nodeRequirement = NodeRequirement(20, 20, 20);
	eval.edgeRequirement = EdgeRequirement(20);

	app.appId = 1;
	app.AddFirstMicroService(0, 1, eval.edgeRequirement);
	for (uint32_t i = 1; i < eval.numMicroServices; i++) {
		app.AddMicroService(i, i + 1, eval.nodeRequirement, eval.edgeRequirement);
	}
	app.AddLastMicroService(eval.numMicroServices, eval.nodeRequirement, eval.edgeRequirement);

	eval.appQos = QoS(1000, 20, 5, 5);

	AgvProfile agvProfile(eval);

	//
	// install app on the cloud manager
	//
	RM_LOG(TEST_LOG, "Created profile " << agvProfile);
	cloudManager.InstallApp(agvProfile);

	//
	// create UE manager
	//
	UeManager ueManager(eval);

	//
	// handle UE join request in cloud
	//
	auto agvs = ueManager.GetAgvs();
	auto oagv = agvs.at(0);
	auto orrh = rrhs.at(0);

	RequestCloud reqCloud(oagv);
	reqCloud.rrhId = orrh.id;
	auto resp = cloudManager.HandleRequest(reqCloud);
	GTEST_ASSERT_EQ(resp.success, true);
	resp = cloudManager.HandleRequest(reqCloud);
	GTEST_ASSERT_EQ(resp.success, true);

	//
	// there should be only one cloud node used
	//
	uint32_t numberFullNodes = 0;
	uint32_t numbeEmptyNodes = 0;
	NodeResource noUsage(0, 0, 0);
	for (uint32_t i = 0; i < eval.numCloudNodes; i++) {
		auto usage = cloudManager.GetNodeUsage(i);
		RM_LOG(TEST_LOG, "Node " << i << " usage " << usage.cpu << "\t" << usage.ram << "\t" << usage.flash);
		if (usage >= eval.nodeCapacity) {
			numberFullNodes++;
		}
		if (noUsage >= usage) {
			numbeEmptyNodes++;
		}
	}
	GTEST_ASSERT_EQ(numberFullNodes == 2, true);
	GTEST_ASSERT_EQ(numbeEmptyNodes == eval.numCloudNodes - numberFullNodes, true);
}

TEST(cloud, deployindepth) {

	EvalProfile eval;
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);
	Cloud cloud(eval);
	cloud.AddNode(0, eval.nodeCapacity);
	cloud.AddNode(1, eval.nodeCapacity);
	cloud.AddNode(2, eval.nodeCapacity);
	uint32_t rrhId = 0;
	cloud.ConnectRrh(0, rrhId);
	cloud.Connect(0, 1, eval.edgeCapacity);
	cloud.Connect(1, 2, eval.edgeCapacity);
	cloud.CalcMaxCloudResources();

	CloudApp app;
	uint32_t numMicroServices = 5; // not counting the fake micro-services
	NodeRequirement nodeRequirement(50, 50, 50);
	EdgeRequirement edgeRequirement(20);
	app.appId = 1;
	app.AddFirstMicroService(0, 1, edgeRequirement);
	for (uint32_t i = 1; i < numMicroServices; i++) {
		app.AddMicroService(i, i + 1, nodeRequirement, edgeRequirement);
	}
	app.AddLastMicroService(numMicroServices, nodeRequirement, edgeRequirement);

	auto success = cloud.DeployApp(app, QoS(1000, 10, 5, 5), rrhId);
	ASSERT_TRUE(success);
	auto usage0 = cloud.GetNodeUsage(0);
	auto usage1 = cloud.GetNodeUsage(1);
	auto usage2 = cloud.GetNodeUsage(2);
	ASSERT_NEAR(usage0.cpu, 100, 0.1);
	ASSERT_NEAR(usage1.cpu, 100, 0.1);
	ASSERT_NEAR(usage2.cpu, 50, 0.1);
	success = cloud.DeployApp(app, QoS(1000, 10, 5, 5), rrhId);
	ASSERT_TRUE(!success);
}

