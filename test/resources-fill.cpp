/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "resource-map.h"

TEST(resources, vacancy) {
	Resources r;
	for (uint32_t i = 0; i <= Carriers::maxRe; i++) {
		r.AddTimeSlot(i);
	}
	for (uint32_t i = 2; i <= Carriers::maxRe; i++) {
		r.AddReservedAt(i, 0);
		r.AddReservedAt(i, i - 1);
	}
	GTEST_ASSERT_EQ(r.GetVacantAt(0), 0);
	GTEST_ASSERT_EQ(r.GetVacantAt(1), 1);
	for (uint32_t i = 2; i <= Carriers::maxRe; i++) {
		GTEST_ASSERT_EQ(r.GetVacantAt(i), i - 2);
	}
}

