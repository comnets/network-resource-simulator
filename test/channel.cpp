/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include <limits>
#include "gtest/gtest.h"
#include "channel.h"
#include "effective-snr.h"
#include "log.h"

TEST(channel, ptp) {

	RanPtpChannel channel;
	Position src(0, 0), dst(0, 2);

	auto snr1 = channel.GetSnr(src, dst);
	dst.y = 20;
	auto snr2 = channel.GetSnr(src, dst);

	GTEST_ASSERT_LE(EffectiveSnr::GetEffSnr(snr2), EffectiveSnr::GetEffSnr(snr1));
}
