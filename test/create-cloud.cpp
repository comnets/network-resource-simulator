/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include "gtest/gtest.h"
#include "cloud-manager.h"
#include "ran-manager.h"
#include "eval-profile.h"

TEST(cloud, manager) {

	EvalProfile eval;

	eval.numRrhs = 4;
	eval.cellSize = 10;
	RanManager ranManager(eval);

	eval.numCloudNodes = 100;
	eval.numConnectionPerCloudNode = 5;
	CloudManager cloudManager(eval, std::bind(&RanManager::GetRRHs, &ranManager));

	for (uint32_t i = 0; i < eval.numCloudNodes; i++) {
		GTEST_ASSERT_EQ(cloudManager.IsNodeConnected(i), true);
	}

	auto rrhs = ranManager.GetRRHs();
	for (auto rrh : rrhs) {
		GTEST_ASSERT_EQ(cloudManager.IsRrhConnected(rrh.id), true);
	}
}

