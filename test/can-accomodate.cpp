/*
 * resources_match_rb.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: tsokalo
 */

#include <limits>
#include "gtest/gtest.h"
#include "cloud-manager.h"
#include "ran-manager.h"
#include "ue-manager.h"

TEST(cloud, accomodate) {

	NodeResource nodeCapacity(50,50,50);
	Performance performance(1000, std::numeric_limits<double>::max(), 5, 5);

	{
		NodeRequirement nodeRequirement(49.999,49.999,49.999);
		CloudNode node(0, nodeCapacity, performance);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), true);
	}

	{
		NodeRequirement nodeRequirement(51,50,50);
		CloudNode node(0, nodeCapacity, performance);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), false);
	}

	{
		NodeRequirement nodeRequirement(50,51,50);
		CloudNode node(0, nodeCapacity, performance);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), false);
	}

	{
		NodeRequirement nodeRequirement(50,50,51);
		CloudNode node(0, nodeCapacity, performance);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), false);
	}

	{
		NodeRequirement nodeRequirement(30,30,30);
		CloudNode node(0, nodeCapacity, performance);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), true);
		node.Assign(nodeRequirement);
		GTEST_ASSERT_EQ(node.CanAccomodate(nodeRequirement), false);
	}

}

