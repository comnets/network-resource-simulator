#include <iostream>
#include <stdint.h>
#include <array>
#include <vector>
#include <map>
#include <random>
#include <iostream>

#include "simulation.h"

int main(int argc, char **argv) {

	EvalProfile eval;
	eval.ReadFromFile("conf.txt");
	eval.numRrhs = 3;

	eval.numAgvs = 60000;
	eval.appQos.jitter = 100000;
	eval.appQos.latency = 0.7;
	eval.appQos.datarate = 50;
	eval.appQos.messageSize = 83;
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.numCloudNodes = 70;
	eval.numConnectionPerCloudNode = 5;
	eval.numMicroServices = 5; // not counting the fake micro-services
	eval.nodeRequirement = NodeRequirement(1, 1, 1);
	eval.edgeRequirement = EdgeRequirement(1);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);

	eval.showNumCloudNodes = true;
	eval.showNumConnPerCloudNode = true;
	eval.showNumMs = true;
	eval.showQos = true;

	std::vector<double> latencies = { 0.7};
	std::vector<double> pamins = {  0.999, 0.99, 0.90, 0.80, 0.70, 0.60, 0.50, 0.40, 0.30, 0.20, 0.10};

	std::uniform_int_distribution<uint32_t> rrhDistr(0, eval.numRrhs - 1);
	std::default_random_engine generator;
	generator.seed(std::chrono::system_clock::now().time_since_epoch().count());

	for (auto latency : latencies) {

		Statistics st;
		uint32_t numIter = NUM_CONF_INTERV_SAMPLES;
		struct StatsPa {
			std::vector<uint32_t> numJoined, numUntilLastJoined, numJoinedUntilPa;
			std::vector<double> cloudUsage;
		};
		std::vector<StatsPa> statsPa(pamins.size(), StatsPa());

		for (uint32_t i = 0; i < numIter; i++) {

			eval.appQos.latency = latency;

			RanManager ran(eval);
			AgvProfile agvProfile(eval);
			CloudManager cloud(eval, std::bind(&RanManager::GetRRHs, &ran));
			cloud.InstallApp(agvProfile);
			ran.InstallApp(agvProfile);
			UeManager uem(eval);

			uint32_t numJoined = 0;
			uint32_t numJoinedUntilPa = 0;
			uint32_t numUntilLastJoined = 0;
			double pa = 0;

			auto pamin = pamins.begin();
			uint32_t c = 0;

			while (!uem.HaveAllTriedToJoin()) {

				c++;

				auto reqMdmo = uem.CreateRequest();
				ReserveOption ranReserveOption;
				ranReserveOption.rrhId = rrhDistr(generator);
				RequestCloud reqCloud(reqMdmo.agv);
				reqCloud.rrhId = ranReserveOption.rrhId;
				auto respCloud = cloud.HandleRequest(reqCloud);

				if (respCloud.success) {
					numJoined++;
					numUntilLastJoined = c;
				}

				pa = (double) numJoined / (double) c;

				if (respCloud.success && pa > *pamin) {
					numJoinedUntilPa = numJoined;
				}
				if (pa < *pamin) {

					statsPa.at(std::distance(pamins.begin(), pamin)).numJoined.push_back(numJoined);
					statsPa.at(std::distance(pamins.begin(), pamin)).numUntilLastJoined.push_back(numUntilLastJoined);
					statsPa.at(std::distance(pamins.begin(), pamin)).numJoinedUntilPa.push_back(numJoinedUntilPa);
					statsPa.at(std::distance(pamins.begin(), pamin)).cloudUsage.push_back(cloud.GetResourceUsage());
//					std::cout << *pamin << "\t" << numJoined << "\t" << numUntilLastJoined << "\t" << numJoinedUntilPa << "\t" << eval << "\t"
//							<< cloud.GetResourceUsage() << std::endl;
					pamin++;
					if (pamin == pamins.end()) {
						break;
					}
				}
			}

		}

		for (uint16_t i = 0; i < pamins.size(); i++) {
			auto numJoined = st.CalcCI(statsPa.at(i).numJoined);
			auto numUntilLastJoined = st.CalcCI(statsPa.at(i).numUntilLastJoined);
			auto numJoinedUntilPa = st.CalcCI(statsPa.at(i).numJoinedUntilPa);
			auto cloudUsage = st.CalcCI(statsPa.at(i).cloudUsage);
			std::cout << pamins.at(i) << "\t" << numJoined << "\t" << numUntilLastJoined << "\t" << numJoinedUntilPa << "\t" << eval << "\t" << cloudUsage
					<< std::endl;
		}
	}
	std::cout << "Finished" << std::endl;

	return 0;
}
