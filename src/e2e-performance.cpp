#include <iostream>
#include <stdint.h>
#include <array>
#include <vector>
#include <map>
#include <random>
#include <iostream>

#include "simulation.h"

int main(int argc, char **argv) {

	EvalProfile eval;
	eval.ReadFromFile("conf.txt");
	eval.numRrhs = 3;
	eval.cellSize = 30;
	eval.numAgvs = 2000;
	eval.appQos.jitter = 100000;
	eval.appQos.latency = 10.0;
	eval.appQos.datarate = 0.04;
	eval.appQos.messageSize = 83;
	eval.qosSplit = QosSplit(2.0, 0.5, 0.5);
	eval.nodeCapacity = NodeResource(100, 100, 100);
	eval.edgeCapacity = EdgeResource(100);
	eval.numCloudNodes = 70;
	eval.numConnectionPerCloudNode = 5;
	eval.numMicroServices = 5; // not counting the fake micro-services
	eval.nodeRequirement = NodeRequirement(1, 1, 1);
	eval.edgeRequirement = EdgeRequirement(1);
	eval.nodePerformance = Performance(0, 3000, 0.1, 0.1);
	eval.edgePerformance = Performance(0, 1000, 0.05, 0.05);
	eval.admissionP = 0.999;

	eval.showNumRrhs = true;
	eval.showNumAgvs = true;
	eval.showCellSize = true;
	eval.showNumCloudNodes = true;
	eval.showNumConnPerCloudNode = true;
	eval.showNumMs = true;
	eval.showQos = true;
	eval.showAdmissionP = true;
	eval.showQosSplit = true;

	std::vector<double> sLatencies = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96};

	eval.admissionP = 0.999;
	eval.qosSplit.sDatarate = 2;
	for (auto sLatency : sLatencies) {

		eval.qosSplit.sLatency = sLatency;

		Simulation sim(eval);
		sim.RunInterative(NUM_CONF_INTERV_SAMPLES);
	}

//	eval.appQos.datarate = 0.04;
//	eval.admissionP = 0.999;
//	eval.qosSplit.sLatency = 0.9;
//	std::vector<double> sDatarates;
//	std::vector<double> ranDatarates = { 0.04005, 0.0401, 0.041, 0.06, 0.08, 0.16, 0.32 };
//	for (auto ranDatarate : ranDatarates) {
//		sDatarates.push_back(ranDatarate / eval.appQos.datarate);
//	}
//
//	for (auto sDatarate : sDatarates) {
//
//		eval.qosSplit.sDatarate = sDatarate;
//
//		Simulation sim(eval);
//		sim.RunInterative(NUM_CONF_INTERV_SAMPLES);
//	}

//	std::vector<double> admissionPs = {  0.999, 0.99, 0.90, 0.80, 0.70, 0.60, 0.50, 0.40, 0.30, 0.20, 0.10 };
//
//
//	eval.qosSplit.sLatency = 0.9;
//	eval.qosSplit.sDatarate = 1.01;
//	for (auto admissionP : admissionPs) {
//
//		eval.admissionP = admissionP;
//
//		Simulation sim(eval);
//		sim.RunInterative(NUM_CONF_INTERV_SAMPLES);
//	}

	std::cout << "Finished" << std::endl;

	return 0;
}
